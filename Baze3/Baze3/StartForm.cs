﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Baze3
{
    public enum Operacija
    {
        Delete,
        Insert,
        Update,
        Show
    }
    public partial class StartForm : Form
    {
      
        private Operacija operacija;
        public UserControl userControl;
        private UserC userC;
        
       
        public StartForm()
        {
            InitializeComponent();
            SidePanel.Hide();
            userControl = null;
            
            foreach (UserControl user in Controls.OfType<UserControl>())
                user.Visible = false;
            pocetniPrikaz1.Visible=true;
            pocetniPrikaz1.BringToFront();
            izaberi1.forma = this;
         
        }
        

        private void panel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void ShowPanel(Button b)
        {
            SidePanel.Show();
            SidePanel.Height = b.Height;
            SidePanel.Top = b.Top;
            izaberi1.Visible = true;
            izaberi1.BringToFront();
        }
        public void PostaviUpdateUserControl(UserControl u)
        {
            u.Visible = true;
            u.BringToFront();
        }
        
        public void PostaviUserControl()
        {
            userC = izaberi1.vratiUserC();
            if (userC != UserC.Nista)
            {
                pocetniPrikaz1.Visible = false;
                dodajDete1.Visible = false;
                switch (userC)
                {
                    case UserC.Hotel:
                        {

                            switch (operacija)
                            {
                                case Operacija.Insert:
                                    {
                                        dodajHotel1.Visible = true;
                                        dodajHotel1.BringToFront();
                                        dodajHotel1.operacija = Operacija.Insert;
                                        dodajHotel1.FormaZaSobu = dodajSobu1;
                                        dodajHotel1.okruzenje = dodajOkruzenje1;
                                        dodajHotel1.EnableSacuvaj();
                                        dodajHotel1.DisableDodajOkruzenju();
                                        dodajHotel1.EnableSacuvaj();
                                    }
                                    break;
                                case Operacija.Delete:
                                    {
                                        showTable1.PrikazHotela();
                                        Obrisi();
                                    }
                                    break;
                                case Operacija.Show:
                                    {
                                        showTable1.PrikazHotela();
                                        Prikazi();
                                    }
                                    break;
                                case Operacija.Update:
                                    {
                                        showTable1.dodajHotel = dodajHotel1;
                                        Izmeni();
                                        showTable1.PrikazHotela();
                                    }
                                    break;                              
                            }
                        }
                        break;
                    case UserC.Apartman:
                        {
                            switch (operacija)
                            {
                                case Operacija.Insert:
                                    {
                                        dodajApartman1.Visible = true;
                                        dodajApartman1.BringToFront();
                                        dodajApartman1.operacija = Operacija.Insert;
                                        dodajApartman1.okruzenje = dodajOkruzenje1;
                                        dodajApartman1.EnableSacuvaj();
                                        dodajApartman1.DisablePovezi();
                                    }
                                    break;
                                case Operacija.Delete: {
                                        showTable1.PrikazApartmana();
                                        Obrisi();
                                    } break;
                                case Operacija.Show:
                                    {
                                        showTable1.PrikazApartmana();
                                        Prikazi();
                                    } break;
                                case Operacija.Update:
                                    {
                                        showTable1.dodajApartman = dodajApartman1;
                                        Izmeni();
                                        showTable1.PrikazApartmana();
                                    }
                                    break;
                            }

                        }
                        break;
                    case UserC.Grupa:
                        {
                            dodajGrupu1.Visible = true;
                            dodajGrupu1.BringToFront();
                            switch (operacija)
                            {
                                case Operacija.Insert:
                                    {
                                        dodajGrupu1.Visible = true;
                                        dodajGrupu1.BringToFront();
                                        dodajGrupu1.operacija = Operacija.Insert;
                                        dodajGrupu1.DisableDodajKlijenta();
                                        dodajGrupu1.EnableSacuvaj();
                                    } break;
                                case Operacija.Delete:
                                    {
                                        showTable1.PrikazGrupe();
                                       Obrisi();
                                    } break;
                                case Operacija.Show:
                                    {
                                        showTable1.PrikazGrupe();
                                        Prikazi();
                                    }
                                    break;
                                case Operacija.Update:
                                    {
                                        //dodajGrupu1.Prikaz(Operacija.Update);
                                        showTable1.dodajGrupu = dodajGrupu1;
                                        Izmeni();
                                        showTable1.PrikazGrupe();
                                    } break;
                            }
                        }
                        break;
                    case UserC.Klijent:
                        {
                            switch (operacija)
                            {
                                case Operacija.Insert:
                                    {
                                        dodajKlijenta1.Visible = true;
                                        dodajKlijenta1.BringToFront();
                                        dodajKlijenta1.operacija = Operacija.Insert;
                                        dodajKlijenta1.Dete = dodajDete1;
                                        dodajKlijenta1.grupa = dodajGrupu1;
                                    }
                                    break;
                                case Operacija.Delete:
                                    {                                  
                                        showTable1.PrikazKlijenta();
                                        Obrisi();
                                    } break;
                                case Operacija.Show:
                                    {
                                        showTable1.PrikazKlijenta();
                                        Prikazi();
                                    }
                                    break;
                                case Operacija.Update:
                                    {
                                        showTable1.dodajKlijenta = dodajKlijenta1;
                                        Izmeni();
                                        showTable1.PrikazKlijenta();
                                    }
                                    break;
                            }
                        }
                        break;
                    case UserC.MedTretman:
                        {
                            switch (operacija)
                            {
                                case Operacija.Insert:
                                    {
                                        dodajMedicinskiTretman1.Visible = true;
                                        dodajMedicinskiTretman1.BringToFront();
                                        dodajMedicinskiTretman1.operacija = Operacija.Insert;
                                        dodajMedicinskiTretman1.okruzenje = dodajOkruzenje1;
                                    }
                                    break;
                                case Operacija.Delete: {
                                        Obrisi();
                                        showTable1.PrikazMedTretmana();
                                        
                                    } break;
                                case Operacija.Show:
                                    {
                                        showTable1.PrikazMedTretmana();
                                        Prikazi();
                                    }
                                    break;
                                case Operacija.Update:
                                    {
                                        showTable1.dodajTretman = dodajMedicinskiTretman1;
                                        Izmeni();
                                        showTable1.PrikazMedTretmana();
                                    } break;
                            }
                        }
                        break;
                    case UserC.Okruzenje:
                        {
                            switch (operacija)
                            {
                                case Operacija.Insert:
                                    {
                                        dodajOkruzenje1.Visible = true;
                                        dodajOkruzenje1.BringToFront();
                                        dodajOkruzenje1.operacija = Operacija.Insert;
                                        dodajOkruzenje1.FormaZaHotel = dodajHotel1;
                                        dodajOkruzenje1.FormaZaApartman = dodajApartman1;
                                        dodajOkruzenje1.tretman = dodajMedicinskiTretman1;
                                        dodajHotel1.FormaZaSobu = dodajSobu1;
                                        dodajHotel1.DisableSacuvaj();
                                    }
                                    break;
                                case Operacija.Delete: {
                                        
                                        showTable1.PrikazOkruzenja();
                                        Obrisi();
                                    } break;
                                case Operacija.Show:
                                    {
                                        showTable1.PrikazOkruzenja();
                                        Prikazi();
                                    }
                                    break;
                                case Operacija.Update:
                                    {
                                        showTable1.dodajOkruzenje = dodajOkruzenje1;
                                        Izmeni();
                                        showTable1.PrikazOkruzenja();
                                    } break;
                            }
                        }
                        break;
                    case UserC.Rezervacija:
                        {
                            switch (operacija)
                            {
                                case Operacija.Insert:
                                    {
                                        dodajRezervaciju1.Visible = true;
                                        dodajRezervaciju1.BringToFront();
                                        dodajRezervaciju1.operacija = Operacija.Insert;
                                        dodajRezervaciju1.tabela = showTable1;
                                        showTable1.dodajRezervaciju = dodajRezervaciju1;
                                    }
                                    break;
                                case Operacija.Delete: {
                                        Obrisi();
                                        showTable1.PrikazRezervacije();
                                        
                                    } break;
                                case Operacija.Show:
                                    {
                                        Prikazi();
                                        showTable1.PrikazRezervacije();
                                    }
                                    break;
                                case Operacija.Update:
                                    {
                                        showTable1.dodajRezervaciju = dodajRezervaciju1;
                                        Izmeni();
                                        showTable1.PrikazRezervacije();
                                    } break;
                            }
                        }
                        break;
                    case UserC.Dete:
                        {
                            switch (operacija)
                            {
                                case Operacija.Insert:
                                    {
                                        dodajDete1.Visible = true;
                                        dodajDete1.BringToFront();
                                        dodajDete1.operacija = Operacija.Insert;
                                    }
                                    break;
                                case Operacija.Delete:
                                    {
                                        Obrisi();
                                        showTable1.PrikazDece();                                       
                                    }
                                    break;
                                case Operacija.Show:
                                    {
                                        Prikazi();
                                        showTable1.PrikazDece();                                   
                                    }
                                    break;
                                case Operacija.Update:
                                    {
                                        showTable1.dodajDete = dodajDete1;
                                        Izmeni();
                                        showTable1.PrikazDece();
                                    }
                                    break;
                            }
                        }
                        break;
                    case UserC.Soba:
                        {
                            switch (operacija)
                            {
                                case Operacija.Insert:
                                    {
                                        dodajSobu1.Visible = true;
                                        dodajSobu1.BringToFront();
                                        dodajSobu1.operacija = Operacija.Insert;
                                        dodajSobu1.Hotel = dodajHotel1;
                                    }
                                    break;
                                case Operacija.Delete:
                                    {
                                        Obrisi();
                                        showTable1.PrikazSoba();                                    
                                    }
                                    break;
                                case Operacija.Show:
                                    {
                                        Prikazi();
                                        showTable1.PrikazSoba();
                                    } break;
                                case Operacija.Update:
                                    {
                                        showTable1.dodajSobu = dodajSobu1;
                                        Izmeni();
                                        showTable1.PrikazSoba();
                                    }
                                    break;
                            }
                        }
                        break;
                    case UserC.Usluga:
                        {
                            switch (operacija)
                            {
                                case Operacija.Insert:
                                    {
                                    }
                                    break;
                                case Operacija.Delete:
                                    {
                                        
                                    }
                                    break;
                                case Operacija.Show:
                                    {
                                        Prikazi();
                                        showTable1.PrikazUsluga();
                                    }
                                    break;
                                case Operacija.Update:
                                    {

                                    }
                                    break;
                            }
                        }
                        break;
                }
            }
        }
        public void Obrisi()
        {
            showTable1.Visible = true;
            showTable1.BringToFront();
            showTable1.Brisem();
        }
        public void Prikazi()
        {
            showTable1.Visible = true;
            showTable1.BringToFront();
            showTable1.Prikaz();
        }
        public void Izmeni()
        {
            showTable1.Visible = true;
            showTable1.BringToFront();
            showTable1.Izmeni();
        }
        private void StartForm_Load(object sender, EventArgs e)
        {
           
        }
        private void exitbtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region Button 

        private void button1_Click(object sender, EventArgs e)
        {
            ShowPanel((Button)sender);
            izaberi1.InsertSoba(true);
            operacija =Operacija.Delete;
            PostaviUserControl();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            operacija = Operacija.Insert;
            ShowPanel((Button)sender);
            PostaviUserControl();
            izaberi1.InsertSoba(false);
        }

        private void updatebtn_Click(object sender, EventArgs e)
        {
            ShowPanel((Button)sender);
            izaberi1.InsertSoba(true);
            operacija = Operacija.Update;
            PostaviUserControl();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            izaberi1.InsertSoba(true);
            ShowPanel((Button)sender);
            operacija = Operacija.Show;
        }

        #endregion



        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pocetniPrikaz1_Load(object sender, EventArgs e)
        {

        }

        private void showTable1_Load(object sender, EventArgs e)
        {

        }
    }
}
