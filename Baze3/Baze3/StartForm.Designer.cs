﻿namespace Baze3
{
    partial class StartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.SidePanel = new System.Windows.Forms.Panel();
            this.button6 = new System.Windows.Forms.Button();
            this.updatebtn = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.exitbtn = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.izaberi1 = new Baze3.Izaberi();
            this.pocetniPrikaz1 = new Baze3.User_Controls.PocetniPrikaz();
            this.dodajApartman1 = new Baze3.DodajApartman();
            this.dodajDete1 = new Baze3.DodajDete();
            this.dodajHotel1 = new Baze3.DodajHotel();
            this.dodajKlijenta1 = new Baze3.DodajKlijenta();
            this.dodajOkruzenje1 = new Baze3.DodajOkruzenje();
            this.dodajGrupu1 = new Baze3.User_Controls.DodajGrupu();
            this.dodajMedicinskiTretman1 = new Baze3.User_Controls.DodajMedicinskiTretman();
            this.dodajRezervaciju1 = new Baze3.User_Controls.DodajRezervaciju();
            this.dodajSobu1 = new Baze3.User_Controls.DodajSobu();
            this.showTable1 = new Baze3.User_Controls.ShowTable();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.panel1.Controls.Add(this.SidePanel);
            this.panel1.Controls.Add(this.button6);
            this.panel1.Controls.Add(this.updatebtn);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(149, 495);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // SidePanel
            // 
            this.SidePanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(8)))), ((int)(((byte)(55)))));
            this.SidePanel.Location = new System.Drawing.Point(2, 45);
            this.SidePanel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.SidePanel.Name = "SidePanel";
            this.SidePanel.Size = new System.Drawing.Size(12, 66);
            this.SidePanel.TabIndex = 3;
            this.SidePanel.Paint += new System.Windows.Forms.PaintEventHandler(this.panel5_Paint);
            // 
            // button6
            // 
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Constantia", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button6.Image = global::Baze3.Properties.Resources.Webp_net_resizeimage__9_;
            this.button6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.Location = new System.Drawing.Point(16, 314);
            this.button6.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(130, 84);
            this.button6.TabIndex = 3;
            this.button6.Text = "   Show";
            this.button6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // updatebtn
            // 
            this.updatebtn.FlatAppearance.BorderSize = 0;
            this.updatebtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.updatebtn.Font = new System.Drawing.Font("Constantia", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updatebtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.updatebtn.Image = global::Baze3.Properties.Resources.Webp_net_resizeimage__8_;
            this.updatebtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.updatebtn.Location = new System.Drawing.Point(16, 224);
            this.updatebtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.updatebtn.Name = "updatebtn";
            this.updatebtn.Size = new System.Drawing.Size(130, 85);
            this.updatebtn.TabIndex = 2;
            this.updatebtn.Text = "    Update";
            this.updatebtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.updatebtn.UseVisualStyleBackColor = true;
            this.updatebtn.Click += new System.EventHandler(this.updatebtn_Click);
            // 
            // button2
            // 
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Constantia", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Image = global::Baze3.Properties.Resources.Webp_net_resizeimage__12_;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(19, 136);
            this.button2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(130, 84);
            this.button2.TabIndex = 1;
            this.button2.Text = "   Create";
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Constantia", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Image = global::Baze3.Properties.Resources.Webp_net_resizeimage__11_;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(19, 45);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(130, 86);
            this.button1.TabIndex = 0;
            this.button1.Text = "  Delete";
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gold;
            this.panel2.Controls.Add(this.exitbtn);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(149, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(548, 40);
            this.panel2.TabIndex = 1;
            // 
            // exitbtn
            // 
            this.exitbtn.BackgroundImage = global::Baze3.Properties.Resources.quit_512;
            this.exitbtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.exitbtn.FlatAppearance.BorderSize = 0;
            this.exitbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exitbtn.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitbtn.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.exitbtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.exitbtn.Location = new System.Drawing.Point(490, 0);
            this.exitbtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.exitbtn.Name = "exitbtn";
            this.exitbtn.Size = new System.Drawing.Size(39, 37);
            this.exitbtn.TabIndex = 5;
            this.exitbtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.exitbtn.UseVisualStyleBackColor = true;
            this.exitbtn.Click += new System.EventHandler(this.exitbtn_Click);
            // 
            // button3
            // 
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(12, 269);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(188, 71);
            this.button3.TabIndex = 2;
            this.button3.Text = "  Delete";
            this.button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button3.UseVisualStyleBackColor = true;
            // 
            // izaberi1
            // 
            this.izaberi1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.izaberi1.forma = null;
            this.izaberi1.Location = new System.Drawing.Point(146, 35);
            this.izaberi1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.izaberi1.Name = "izaberi1";
            this.izaberi1.Size = new System.Drawing.Size(180, 461);
            this.izaberi1.TabIndex = 4;
            // 
            // pocetniPrikaz1
            // 
            this.pocetniPrikaz1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.pocetniPrikaz1.Location = new System.Drawing.Point(147, 37);
            this.pocetniPrikaz1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pocetniPrikaz1.Name = "pocetniPrikaz1";
            this.pocetniPrikaz1.Size = new System.Drawing.Size(550, 458);
            this.pocetniPrikaz1.TabIndex = 5;
            // 
            // dodajApartman1
            // 
            this.dodajApartman1.BackColor = System.Drawing.Color.LavenderBlush;
            this.dodajApartman1.Location = new System.Drawing.Point(332, 37);
            this.dodajApartman1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dodajApartman1.Name = "dodajApartman1";
            this.dodajApartman1.operacija = Baze3.Operacija.Delete;
            this.dodajApartman1.Size = new System.Drawing.Size(368, 455);
            this.dodajApartman1.TabIndex = 6;
            // 
            // dodajDete1
            // 
            this.dodajDete1.Location = new System.Drawing.Point(332, 37);
            this.dodajDete1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dodajDete1.Name = "dodajDete1";
            this.dodajDete1.operacija = Baze3.Operacija.Delete;
            this.dodajDete1.Size = new System.Drawing.Size(368, 455);
            this.dodajDete1.TabIndex = 7;
            // 
            // dodajHotel1
            // 
            this.dodajHotel1.BackColor = System.Drawing.Color.Wheat;
            this.dodajHotel1.HotelId = 0;
            this.dodajHotel1.Location = new System.Drawing.Point(332, 37);
            this.dodajHotel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dodajHotel1.Name = "dodajHotel1";
            this.dodajHotel1.operacija = Baze3.Operacija.Delete;
            this.dodajHotel1.Size = new System.Drawing.Size(368, 455);
            this.dodajHotel1.TabIndex = 8;
            // 
            // dodajKlijenta1
            // 
            this.dodajKlijenta1.BackColor = System.Drawing.Color.LightCyan;
            this.dodajKlijenta1.Location = new System.Drawing.Point(332, 37);
            this.dodajKlijenta1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dodajKlijenta1.Name = "dodajKlijenta1";
            this.dodajKlijenta1.operacija = Baze3.Operacija.Delete;
            this.dodajKlijenta1.Size = new System.Drawing.Size(368, 455);
            this.dodajKlijenta1.TabIndex = 9;
            // 
            // dodajOkruzenje1
            // 
            this.dodajOkruzenje1.BackColor = System.Drawing.Color.PeachPuff;
            this.dodajOkruzenje1.Location = new System.Drawing.Point(332, 37);
            this.dodajOkruzenje1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dodajOkruzenje1.Name = "dodajOkruzenje1";
            this.dodajOkruzenje1.operacija = Baze3.Operacija.Delete;
            this.dodajOkruzenje1.Size = new System.Drawing.Size(368, 455);
            this.dodajOkruzenje1.TabIndex = 10;
            // 
            // dodajGrupu1
            // 
            this.dodajGrupu1.BackColor = System.Drawing.Color.RosyBrown;
            this.dodajGrupu1.Location = new System.Drawing.Point(332, 37);
            this.dodajGrupu1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dodajGrupu1.Name = "dodajGrupu1";
            this.dodajGrupu1.Size = new System.Drawing.Size(368, 455);
            this.dodajGrupu1.TabIndex = 11;
            // 
            // dodajMedicinskiTretman1
            // 
            this.dodajMedicinskiTretman1.BackColor = System.Drawing.Color.DarkSalmon;
            this.dodajMedicinskiTretman1.Location = new System.Drawing.Point(332, 37);
            this.dodajMedicinskiTretman1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dodajMedicinskiTretman1.Name = "dodajMedicinskiTretman1";
            this.dodajMedicinskiTretman1.operacija = Baze3.Operacija.Delete;
            this.dodajMedicinskiTretman1.Size = new System.Drawing.Size(368, 455);
            this.dodajMedicinskiTretman1.TabIndex = 12;
            // 
            // dodajRezervaciju1
            // 
            this.dodajRezervaciju1.BackColor = System.Drawing.Color.Wheat;
            this.dodajRezervaciju1.Location = new System.Drawing.Point(332, 37);
            this.dodajRezervaciju1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dodajRezervaciju1.Name = "dodajRezervaciju1";
            this.dodajRezervaciju1.operacija = Baze3.Operacija.Delete;
            this.dodajRezervaciju1.Size = new System.Drawing.Size(368, 455);
            this.dodajRezervaciju1.TabIndex = 13;
            // 
            // dodajSobu1
            // 
            this.dodajSobu1.Location = new System.Drawing.Point(332, 37);
            this.dodajSobu1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dodajSobu1.Name = "dodajSobu1";
            this.dodajSobu1.operacija = Baze3.Operacija.Delete;
            this.dodajSobu1.Size = new System.Drawing.Size(368, 455);
            this.dodajSobu1.TabIndex = 14;
            // 
            // showTable1
            // 
            this.showTable1.dodajHotel = null;
            this.showTable1.Location = new System.Drawing.Point(332, 37);
            this.showTable1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.showTable1.Name = "showTable1";
            this.showTable1.Size = new System.Drawing.Size(368, 455);
            this.showTable1.TabIndex = 15;
            this.showTable1.tipOperacije = Baze3.UserC.Nista;
            this.showTable1.Load += new System.EventHandler(this.showTable1_Load);
            // 
            // StartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ClientSize = new System.Drawing.Size(697, 495);
            this.Controls.Add(this.showTable1);
            this.Controls.Add(this.dodajSobu1);
            this.Controls.Add(this.dodajRezervaciju1);
            this.Controls.Add(this.dodajMedicinskiTretman1);
            this.Controls.Add(this.dodajGrupu1);
            this.Controls.Add(this.dodajOkruzenje1);
            this.Controls.Add(this.dodajKlijenta1);
            this.Controls.Add(this.dodajHotel1);
            this.Controls.Add(this.dodajDete1);
            this.Controls.Add(this.dodajApartman1);
            this.Controls.Add(this.pocetniPrikaz1);
            this.Controls.Add(this.izaberi1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "StartForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "StartForm";
            this.Load += new System.EventHandler(this.StartForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button updatebtn;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button exitbtn;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Panel SidePanel;
        private Izaberi izaberi1;
      
        private User_Controls.PocetniPrikaz pocetniPrikaz1;
        private DodajApartman dodajApartman1;
        private DodajDete dodajDete1;
        private DodajHotel dodajHotel1;
        private DodajKlijenta dodajKlijenta1;
        private DodajOkruzenje dodajOkruzenje1;
        private User_Controls.DodajGrupu dodajGrupu1;
        private User_Controls.DodajMedicinskiTretman dodajMedicinskiTretman1;
        private User_Controls.DodajRezervaciju dodajRezervaciju1;
        private User_Controls.DodajSobu dodajSobu1;

        private User_Controls.ShowTable showTable1;
    }
}