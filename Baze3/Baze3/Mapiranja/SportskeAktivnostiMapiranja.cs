﻿using Baze3.Entiteti;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Baze3.Mapiranja
{
    class SportskeAktivnostiMapiranja : ClassMap<SportskeAktivnosti>
    {
        public SportskeAktivnostiMapiranja()
        {
            Table("SPORTSKE_AKTIVNOSTI");

            Id(x => x.Id, "ID").GeneratedBy.SequenceIdentity("S16116.S_SPORT");

            Map(x => x.Aktivnost, "AKTIVNOST");

            References(x => x.PripadaHotelu).Column("ID_HOTELA");
        }
    }
}
