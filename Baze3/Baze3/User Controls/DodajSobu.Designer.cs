﻿namespace Baze3.User_Controls
{
    partial class DodajSobu
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.Klijentbtn = new System.Windows.Forms.Button();
            this.chbSpavanje = new System.Windows.Forms.CheckBox();
            this.chbPolupansion = new System.Windows.Forms.CheckBox();
            this.chbPansion = new System.Windows.Forms.CheckBox();
            this.txtPansionCena = new System.Windows.Forms.TextBox();
            this.txtSpavanjeCena = new System.Windows.Forms.TextBox();
            this.txtPopupansionCena = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(219, 69);
            this.radioButton3.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(83, 17);
            this.radioButton3.TabIndex = 3;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Trokrevetna";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(32, 69);
            this.radioButton2.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(96, 17);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Jednokrevetna";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(129, 69);
            this.radioButton1.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(87, 17);
            this.radioButton1.TabIndex = 2;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Dvokrevetna";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(29, 38);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 17);
            this.label1.TabIndex = 12;
            this.label1.Text = "Odaberite tip sobe:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(29, 247);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 17);
            this.label5.TabIndex = 16;
            this.label5.Text = "Broj sobe";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(29, 269);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 17);
            this.label6.TabIndex = 17;
            this.label6.Text = "Sprat";
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.CheckOnClick = true;
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Items.AddRange(new object[] {
            "TV",
            "Pogled na more",
            "Radio",
            "Kupatilo",
            "Klima",
            "Slobodna",
            "Frizider"});
            this.checkedListBox1.Location = new System.Drawing.Point(155, 113);
            this.checkedListBox1.Margin = new System.Windows.Forms.Padding(2);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(134, 109);
            this.checkedListBox1.TabIndex = 4;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(155, 245);
            this.textBox1.Margin = new System.Windows.Forms.Padding(2);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(76, 20);
            this.textBox1.TabIndex = 5;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(155, 269);
            this.textBox2.Margin = new System.Windows.Forms.Padding(2);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(76, 20);
            this.textBox2.TabIndex = 6;
            // 
            // Klijentbtn
            // 
            this.Klijentbtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(8)))), ((int)(((byte)(55)))));
            this.Klijentbtn.Location = new System.Drawing.Point(261, 390);
            this.Klijentbtn.Margin = new System.Windows.Forms.Padding(2);
            this.Klijentbtn.Name = "Klijentbtn";
            this.Klijentbtn.Size = new System.Drawing.Size(88, 37);
            this.Klijentbtn.TabIndex = 7;
            this.Klijentbtn.Text = "Unesi podatke";
            this.Klijentbtn.UseVisualStyleBackColor = false;
            this.Klijentbtn.Click += new System.EventHandler(this.Klijentbtn_Click);
            // 
            // chbSpavanje
            // 
            this.chbSpavanje.AutoSize = true;
            this.chbSpavanje.Location = new System.Drawing.Point(35, 358);
            this.chbSpavanje.Name = "chbSpavanje";
            this.chbSpavanje.Size = new System.Drawing.Size(71, 17);
            this.chbSpavanje.TabIndex = 18;
            this.chbSpavanje.Text = "Spavanje";
            this.chbSpavanje.UseVisualStyleBackColor = true;
            // 
            // chbPolupansion
            // 
            this.chbPolupansion.AutoSize = true;
            this.chbPolupansion.Location = new System.Drawing.Point(35, 384);
            this.chbPolupansion.Name = "chbPolupansion";
            this.chbPolupansion.Size = new System.Drawing.Size(84, 17);
            this.chbPolupansion.TabIndex = 19;
            this.chbPolupansion.Text = "Polupansion";
            this.chbPolupansion.UseVisualStyleBackColor = true;
            // 
            // chbPansion
            // 
            this.chbPansion.AutoSize = true;
            this.chbPansion.Location = new System.Drawing.Point(35, 407);
            this.chbPansion.Name = "chbPansion";
            this.chbPansion.Size = new System.Drawing.Size(64, 17);
            this.chbPansion.TabIndex = 20;
            this.chbPansion.Text = "Pansion";
            this.chbPansion.UseVisualStyleBackColor = true;
            // 
            // txtPansionCena
            // 
            this.txtPansionCena.Location = new System.Drawing.Point(129, 407);
            this.txtPansionCena.Name = "txtPansionCena";
            this.txtPansionCena.Size = new System.Drawing.Size(55, 20);
            this.txtPansionCena.TabIndex = 23;
            // 
            // txtSpavanjeCena
            // 
            this.txtSpavanjeCena.Location = new System.Drawing.Point(129, 355);
            this.txtSpavanjeCena.Name = "txtSpavanjeCena";
            this.txtSpavanjeCena.Size = new System.Drawing.Size(55, 20);
            this.txtSpavanjeCena.TabIndex = 24;
            // 
            // txtPopupansionCena
            // 
            this.txtPopupansionCena.Location = new System.Drawing.Point(129, 381);
            this.txtPopupansionCena.Name = "txtPopupansionCena";
            this.txtPopupansionCena.Size = new System.Drawing.Size(55, 20);
            this.txtPopupansionCena.TabIndex = 25;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 339);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(173, 13);
            this.label2.TabIndex = 26;
            this.label2.Text = "Odaberite tip usluge i unesite cenu:";
            // 
            // DodajSobu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtPopupansionCena);
            this.Controls.Add(this.txtSpavanjeCena);
            this.Controls.Add(this.txtPansionCena);
            this.Controls.Add(this.chbPansion);
            this.Controls.Add(this.chbPolupansion);
            this.Controls.Add(this.chbSpavanje);
            this.Controls.Add(this.Klijentbtn);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.checkedListBox1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.radioButton3);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "DodajSobu";
            this.Size = new System.Drawing.Size(368, 455);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button Klijentbtn;
        private System.Windows.Forms.CheckBox chbSpavanje;
        private System.Windows.Forms.CheckBox chbPolupansion;
        private System.Windows.Forms.CheckBox chbPansion;
        private System.Windows.Forms.TextBox txtPansionCena;
        private System.Windows.Forms.TextBox txtSpavanjeCena;
        private System.Windows.Forms.TextBox txtPopupansionCena;
        private System.Windows.Forms.Label label2;
    }
}
