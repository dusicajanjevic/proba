﻿namespace Baze3
{
    partial class DodajOkruzenje
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.okruzenjeGB = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.btnDodajHotel = new System.Windows.Forms.Button();
            this.btnDodajTretman = new System.Windows.Forms.Button();
            this.btnDodajApartman = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.okruzenjeGB.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Location = new System.Drawing.Point(49, 144);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(262, 115);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dodatne usluge okruzenja";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.RosyBrown;
            this.button1.Location = new System.Drawing.Point(84, 67);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(131, 31);
            this.button1.TabIndex = 7;
            this.button1.Text = "Dodaj dodatnu uslugu ";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(31, 33);
            this.textBox1.Margin = new System.Windows.Forms.Padding(2);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(184, 20);
            this.textBox1.TabIndex = 0;
            // 
            // okruzenjeGB
            // 
            this.okruzenjeGB.Controls.Add(this.comboBox1);
            this.okruzenjeGB.Controls.Add(this.label1);
            this.okruzenjeGB.Location = new System.Drawing.Point(49, 55);
            this.okruzenjeGB.Margin = new System.Windows.Forms.Padding(2);
            this.okruzenjeGB.Name = "okruzenjeGB";
            this.okruzenjeGB.Padding = new System.Windows.Forms.Padding(2);
            this.okruzenjeGB.Size = new System.Drawing.Size(262, 66);
            this.okruzenjeGB.TabIndex = 15;
            this.okruzenjeGB.TabStop = false;
            this.okruzenjeGB.Text = "Okruzenje";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Banja",
            "Spa centar",
            "Banja sa spa centrom"});
            this.comboBox1.Location = new System.Drawing.Point(105, 31);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(117, 21);
            this.comboBox1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 33);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Tip okruzenja";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.RosyBrown;
            this.button2.Location = new System.Drawing.Point(211, 342);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 50);
            this.button2.TabIndex = 14;
            this.button2.Text = "Sacuvaj promene";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnDodajHotel
            // 
            this.btnDodajHotel.BackColor = System.Drawing.Color.RosyBrown;
            this.btnDodajHotel.Location = new System.Drawing.Point(49, 276);
            this.btnDodajHotel.Name = "btnDodajHotel";
            this.btnDodajHotel.Size = new System.Drawing.Size(100, 47);
            this.btnDodajHotel.TabIndex = 17;
            this.btnDodajHotel.Text = "Dodaj hotel";
            this.btnDodajHotel.UseVisualStyleBackColor = false;
            this.btnDodajHotel.Click += new System.EventHandler(this.btnDodajHotel_Click);
            // 
            // btnDodajTretman
            // 
            this.btnDodajTretman.BackColor = System.Drawing.Color.RosyBrown;
            this.btnDodajTretman.Location = new System.Drawing.Point(49, 342);
            this.btnDodajTretman.Name = "btnDodajTretman";
            this.btnDodajTretman.Size = new System.Drawing.Size(100, 50);
            this.btnDodajTretman.TabIndex = 18;
            this.btnDodajTretman.Text = "Dodaj medicinski tretman";
            this.btnDodajTretman.UseVisualStyleBackColor = false;
            this.btnDodajTretman.Click += new System.EventHandler(this.btnDodajTretman_Click);
            // 
            // btnDodajApartman
            // 
            this.btnDodajApartman.BackColor = System.Drawing.Color.RosyBrown;
            this.btnDodajApartman.Location = new System.Drawing.Point(211, 276);
            this.btnDodajApartman.Name = "btnDodajApartman";
            this.btnDodajApartman.Size = new System.Drawing.Size(100, 47);
            this.btnDodajApartman.TabIndex = 19;
            this.btnDodajApartman.Text = "Dodaj apartman";
            this.btnDodajApartman.UseVisualStyleBackColor = false;
            this.btnDodajApartman.Click += new System.EventHandler(this.btnDodajApartman_Click);
            // 
            // DodajOkruzenje
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PeachPuff;
            this.Controls.Add(this.btnDodajApartman);
            this.Controls.Add(this.btnDodajTretman);
            this.Controls.Add(this.btnDodajHotel);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.okruzenjeGB);
            this.Controls.Add(this.button2);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "DodajOkruzenje";
            this.Size = new System.Drawing.Size(368, 455);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.okruzenjeGB.ResumeLayout(false);
            this.okruzenjeGB.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.GroupBox okruzenjeGB;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnDodajHotel;
        private System.Windows.Forms.Button btnDodajTretman;
        private System.Windows.Forms.Button btnDodajApartman;
    }
}
