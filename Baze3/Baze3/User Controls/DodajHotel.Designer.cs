﻿namespace Baze3
{
    partial class DodajHotel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.restoranHoteltxt = new System.Windows.Forms.TextBox();
            this.dirHoteltxt = new System.Windows.Forms.TextBox();
            this.brLezajaHoteltxt = new System.Windows.Forms.TextBox();
            this.kategorijaHoteltxt = new System.Windows.Forms.TextBox();
            this.gradHoteltxt = new System.Windows.Forms.TextBox();
            this.adresaHoteltxt = new System.Windows.Forms.TextBox();
            this.sifraHoteltxt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.imeHoteltxt = new System.Windows.Forms.TextBox();
            this.sportbtn = new System.Windows.Forms.Button();
            this.sacuvajbtn = new System.Windows.Forms.Button();
            this.sporttxt = new System.Windows.Forms.TextBox();
            this.btnSoba = new System.Windows.Forms.Button();
            this.btnDodajOkruzenju = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.restoranHoteltxt);
            this.groupBox2.Controls.Add(this.dirHoteltxt);
            this.groupBox2.Controls.Add(this.brLezajaHoteltxt);
            this.groupBox2.Controls.Add(this.kategorijaHoteltxt);
            this.groupBox2.Controls.Add(this.gradHoteltxt);
            this.groupBox2.Controls.Add(this.adresaHoteltxt);
            this.groupBox2.Controls.Add(this.sifraHoteltxt);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.imeHoteltxt);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(10, 11);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(196, 344);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Hotel";
            // 
            // restoranHoteltxt
            // 
            this.restoranHoteltxt.Location = new System.Drawing.Point(160, 311);
            this.restoranHoteltxt.Margin = new System.Windows.Forms.Padding(2);
            this.restoranHoteltxt.Name = "restoranHoteltxt";
            this.restoranHoteltxt.Size = new System.Drawing.Size(32, 23);
            this.restoranHoteltxt.TabIndex = 8;
            // 
            // dirHoteltxt
            // 
            this.dirHoteltxt.Location = new System.Drawing.Point(94, 254);
            this.dirHoteltxt.Margin = new System.Windows.Forms.Padding(2);
            this.dirHoteltxt.Name = "dirHoteltxt";
            this.dirHoteltxt.Size = new System.Drawing.Size(76, 23);
            this.dirHoteltxt.TabIndex = 7;
            // 
            // brLezajaHoteltxt
            // 
            this.brLezajaHoteltxt.Location = new System.Drawing.Point(94, 213);
            this.brLezajaHoteltxt.Margin = new System.Windows.Forms.Padding(2);
            this.brLezajaHoteltxt.Name = "brLezajaHoteltxt";
            this.brLezajaHoteltxt.Size = new System.Drawing.Size(76, 23);
            this.brLezajaHoteltxt.TabIndex = 6;
            // 
            // kategorijaHoteltxt
            // 
            this.kategorijaHoteltxt.Location = new System.Drawing.Point(94, 170);
            this.kategorijaHoteltxt.Margin = new System.Windows.Forms.Padding(2);
            this.kategorijaHoteltxt.Name = "kategorijaHoteltxt";
            this.kategorijaHoteltxt.Size = new System.Drawing.Size(76, 23);
            this.kategorijaHoteltxt.TabIndex = 5;
            // 
            // gradHoteltxt
            // 
            this.gradHoteltxt.Location = new System.Drawing.Point(94, 132);
            this.gradHoteltxt.Margin = new System.Windows.Forms.Padding(2);
            this.gradHoteltxt.Name = "gradHoteltxt";
            this.gradHoteltxt.Size = new System.Drawing.Size(76, 23);
            this.gradHoteltxt.TabIndex = 4;
            // 
            // adresaHoteltxt
            // 
            this.adresaHoteltxt.Location = new System.Drawing.Point(94, 98);
            this.adresaHoteltxt.Margin = new System.Windows.Forms.Padding(2);
            this.adresaHoteltxt.Name = "adresaHoteltxt";
            this.adresaHoteltxt.Size = new System.Drawing.Size(76, 23);
            this.adresaHoteltxt.TabIndex = 3;
            // 
            // sifraHoteltxt
            // 
            this.sifraHoteltxt.Location = new System.Drawing.Point(94, 65);
            this.sifraHoteltxt.Margin = new System.Windows.Forms.Padding(2);
            this.sifraHoteltxt.Name = "sifraHoteltxt";
            this.sifraHoteltxt.Size = new System.Drawing.Size(76, 23);
            this.sifraHoteltxt.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 132);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Grad";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 65);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 17);
            this.label8.TabIndex = 7;
            this.label8.Text = "Sifra";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 31);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 17);
            this.label7.TabIndex = 6;
            this.label7.Text = "Ime";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 259);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "Direktor";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 172);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Kategorija";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 215);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Broj lezaja";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 98);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Adresa";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 314);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(152, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Broj mesta u restoranu";
            // 
            // imeHoteltxt
            // 
            this.imeHoteltxt.Location = new System.Drawing.Point(94, 31);
            this.imeHoteltxt.Margin = new System.Windows.Forms.Padding(2);
            this.imeHoteltxt.Name = "imeHoteltxt";
            this.imeHoteltxt.Size = new System.Drawing.Size(76, 23);
            this.imeHoteltxt.TabIndex = 1;
            // 
            // sportbtn
            // 
            this.sportbtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(8)))), ((int)(((byte)(55)))));
            this.sportbtn.Location = new System.Drawing.Point(210, 109);
            this.sportbtn.Margin = new System.Windows.Forms.Padding(2);
            this.sportbtn.Name = "sportbtn";
            this.sportbtn.Size = new System.Drawing.Size(147, 27);
            this.sportbtn.TabIndex = 21;
            this.sportbtn.Text = "Dodaj Sportsku aktivnost";
            this.sportbtn.UseVisualStyleBackColor = false;
            this.sportbtn.Click += new System.EventHandler(this.button2_Click);
            // 
            // sacuvajbtn
            // 
            this.sacuvajbtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(8)))), ((int)(((byte)(55)))));
            this.sacuvajbtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sacuvajbtn.Location = new System.Drawing.Point(218, 409);
            this.sacuvajbtn.Margin = new System.Windows.Forms.Padding(2);
            this.sacuvajbtn.Name = "sacuvajbtn";
            this.sacuvajbtn.Size = new System.Drawing.Size(147, 33);
            this.sacuvajbtn.TabIndex = 15;
            this.sacuvajbtn.Text = "Sacuvaj";
            this.sacuvajbtn.UseVisualStyleBackColor = false;
            this.sacuvajbtn.Click += new System.EventHandler(this.sacuvajbtn_Click);
            // 
            // sporttxt
            // 
            this.sporttxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sporttxt.Location = new System.Drawing.Point(209, 143);
            this.sporttxt.Margin = new System.Windows.Forms.Padding(2);
            this.sporttxt.Multiline = true;
            this.sporttxt.Name = "sporttxt";
            this.sporttxt.Size = new System.Drawing.Size(148, 36);
            this.sporttxt.TabIndex = 12;
            // 
            // btnSoba
            // 
            this.btnSoba.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(8)))), ((int)(((byte)(55)))));
            this.btnSoba.Location = new System.Drawing.Point(17, 364);
            this.btnSoba.Name = "btnSoba";
            this.btnSoba.Size = new System.Drawing.Size(135, 33);
            this.btnSoba.TabIndex = 13;
            this.btnSoba.Text = "Dodaj sobu";
            this.btnSoba.UseVisualStyleBackColor = false;
            this.btnSoba.Click += new System.EventHandler(this.btnSoba_Click);
            // 
            // btnDodajOkruzenju
            // 
            this.btnDodajOkruzenju.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(8)))), ((int)(((byte)(55)))));
            this.btnDodajOkruzenju.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDodajOkruzenju.Location = new System.Drawing.Point(218, 364);
            this.btnDodajOkruzenju.Margin = new System.Windows.Forms.Padding(2);
            this.btnDodajOkruzenju.Name = "btnDodajOkruzenju";
            this.btnDodajOkruzenju.Size = new System.Drawing.Size(147, 33);
            this.btnDodajOkruzenju.TabIndex = 14;
            this.btnDodajOkruzenju.Text = "Dodaj okruzenju";
            this.btnDodajOkruzenju.UseVisualStyleBackColor = false;
            this.btnDodajOkruzenju.Click += new System.EventHandler(this.btnDodajOkruzenju_Click);
            // 
            // DodajHotel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Wheat;
            this.Controls.Add(this.btnDodajOkruzenju);
            this.Controls.Add(this.btnSoba);
            this.Controls.Add(this.sporttxt);
            this.Controls.Add(this.sportbtn);
            this.Controls.Add(this.sacuvajbtn);
            this.Controls.Add(this.groupBox2);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "DodajHotel";
            this.Size = new System.Drawing.Size(368, 455);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox restoranHoteltxt;
        private System.Windows.Forms.Button sportbtn;
        private System.Windows.Forms.TextBox dirHoteltxt;
        private System.Windows.Forms.TextBox brLezajaHoteltxt;
        private System.Windows.Forms.TextBox kategorijaHoteltxt;
        private System.Windows.Forms.TextBox gradHoteltxt;
        private System.Windows.Forms.TextBox adresaHoteltxt;
        private System.Windows.Forms.TextBox sifraHoteltxt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox imeHoteltxt;
        private System.Windows.Forms.Button sacuvajbtn;
        private System.Windows.Forms.TextBox sporttxt;
        private System.Windows.Forms.Button btnSoba;
        private System.Windows.Forms.Button btnDodajOkruzenju;
    }
}
