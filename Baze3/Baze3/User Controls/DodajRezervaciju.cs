﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Baze3.User_Controls
{
    public partial class DodajRezervaciju : UserControl
    {
        public Operacija operacija { get; set; }
        public int RezervacijaId;
        Provajderi.RezervacijaProvajder rezervacijaProvajder;
        public ShowTable tabela { get; set; }
        public Entiteti.Klijent klijentRez;
        public Entiteti.Soba sobaRez;
        public Entiteti.Apartman apartmanRez;

        public DodajRezervaciju()
        {
            InitializeComponent();
            rezervacijaProvajder = new Provajderi.RezervacijaProvajder();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(tipRezcbx.SelectedItem.Equals("Rezervacija hotela"))
            {
                rezHtxt.Visible = true;
                rezHlbl.Visible = true;
            }
            else
            {            
                rezHtxt.Visible = false;
                rezHlbl.Visible = false;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Entiteti.Rezervacija r;
            string tip;
            if (tipRezcbx.Text == "Rezervacija hotela")
                tip = "HOTEL";
            else
            {
                tip = "APARTMAN";
                rezHlbl.Visible = false;
                rezHtxt.Visible = false;
            }

            if (tip == "APARTMAN")
            {
                r = new Entiteti.Rezervacija()
                {
                    TipRezervacije = tip,
                    Od = DateTime.Parse(dateTimePicker1.Text),
                    Do = DateTime.Parse(dateTimePicker2.Text)
                };
            }
            else
            {
                r = new Entiteti.RezervacijaHotela()
                {
                    TipRezervacije = tip,
                    Od = DateTime.Parse(dateTimePicker1.Text),
                    Do = DateTime.Parse(dateTimePicker2.Text),
                    TipUsluge = rezHtxt.Text
                };
            }

            switch (operacija)
            {
                case Operacija.Insert:
                    {
                        if (rezervacijaProvajder.AddRezervacija(r)==1)
                            MessageBox.Show("Uspesan insert rezervacije.");
                        else
                            MessageBox.Show("Neuspesan insert rezervacije!");
                        
                         if(rezervacijaProvajder.PraviRez(klijentRez, r))
                            MessageBox.Show("Uspesan insert PraviRez.");
                         else
                            MessageBox.Show("Neuspesan insert PraviRez!");
                        
                    } break;
                case Operacija.Update:
                    {
                        if (rezervacijaProvajder.UpdateRezervacija(RezervacijaId, r))
                            MessageBox.Show("Uspesan update.");
                        else
                            MessageBox.Show("Neuspesan update!");
                    } break;
            }
        }

        private void rezHtxt_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            tabela.PrikazKlijenta();
            tabela.tip = 'k';
            tabela.Visible = true;
            tabela.BringToFront();
            tabela.Odabir();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (tipRezcbx.Text == "Rezervacija hotela")
            {
                tabela.PrikazSoba();
                tabela.tip = 'h';
            }
            else
            {
                tabela.PrikazApartmana();
                tabela.tip = 'a';
            }
            tabela.Visible = true;
            tabela.BringToFront();
            tabela.Odabir();
        }
    }
}
