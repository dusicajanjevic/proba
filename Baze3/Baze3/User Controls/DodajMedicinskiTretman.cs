﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Baze3.User_Controls
{
    public partial class DodajMedicinskiTretman : UserControl
    {
        public Operacija operacija { get; set; }
        Provajderi.MedicinskiTretmanProvajder tretmanProvajder;
        public int TretmanId { get; set; }
        public DodajOkruzenje okruzenje { get; set; }
        Entiteti.MedicinskiTretman med;

        public DodajMedicinskiTretman()
        {
            InitializeComponent();
            tretmanProvajder = new Provajderi.MedicinskiTretmanProvajder();
        }

        private void medBG_Enter(object sender, EventArgs e)
        {

        }

        private void NapraviTretman()
        {
            med = new Entiteti.MedicinskiTretman()
            {
                ImeLekara = textBox1.Text,
                Specijalnost = textBox2.Text
            };
        }
        private void Klijentbtn_Click(object sender, EventArgs e)
        {
            NapraviTretman();
            switch (operacija)
            {
                case Operacija.Insert:
                    {
                        if(tretmanProvajder.AddMedicinskiTretman(med)==1)
                            MessageBox.Show("Uspesan insert.");
                        else
                            MessageBox.Show("Neuspesan insert!");
                    } break;
                case Operacija.Update:
                    {
                        if(tretmanProvajder.UpdateMedicinskiTretman(TretmanId, med))
                            MessageBox.Show("Uspesan update.");
                        else
                            MessageBox.Show("Neuspesan update!");
                    } break;
            }
        }

        private void btnDodajTretman_Click(object sender, EventArgs e)
        {
            NapraviTretman();
            okruzenje.DodajTretman(med);
            MessageBox.Show("Medicinski tretman je dodat okruzenju.");
            okruzenje.Visible = true;
            okruzenje.BringToFront();
            Klijentbtn.Visible = true;
        }

        public void DisableSacuvaj()
        {
            Klijentbtn.Visible = false;
        }
        public void EnableSacuvaj()
        {
            Klijentbtn.Visible = true;
        }
        public void DisablePovezi()
        {
            btnDodajTretman.Visible = false;
        }
        public void EnablePovezi()
        {
            btnDodajTretman.Visible = true;
        }
    }
}
