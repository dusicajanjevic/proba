﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Baze3
{
    public enum UserC
    {
        Nista,
        Hotel,
        Apartman,
        Dete,
        Grupa,
        Klijent,
        MedTretman,
        Okruzenje,
        Rezervacija, 
        Soba, 
        Usluga
    }

    public partial class Izaberi : UserControl
    {
    //    private Operacija operacija;
        private UserC userC;
        public StartForm forma { get; set; }

        public Izaberi()
        { 
            InitializeComponent();
            SidePanel.Hide();
            
        }
        public void ShowPanel(Button b)
        {
            SidePanel.Show();
            SidePanel.Height = b.Height;
            SidePanel.Top = b.Top;
            
        }

        public UserC vratiUserC()
        {
            return userC;
        }
        private void button4_Click(object sender, EventArgs e)
        {
            ShowPanel((Button)sender);
            userC = UserC.Hotel;
            forma.PostaviUserControl();

        }

        private void apartmanbtn_Click(object sender, EventArgs e)
        {
            ShowPanel((Button)sender);
            userC = UserC.Apartman;
            forma.PostaviUserControl();
        }

        private void medtretmanbtn_Click(object sender, EventArgs e)
        {
            ShowPanel((Button)sender);
            userC = UserC.MedTretman;
            forma.PostaviUserControl();
        }

        private void okruzenjebtn_Click(object sender, EventArgs e)
        {
            ShowPanel((Button)sender);
            userC = UserC.Okruzenje;
            forma.PostaviUserControl();
        }

        private void rezervacijabtn_Click(object sender, EventArgs e)
        {
            ShowPanel((Button)sender);
            userC = UserC.Rezervacija;
            forma.PostaviUserControl();
        }

        private void grupabtn_Click(object sender, EventArgs e)
        {
            ShowPanel((Button)sender);
            userC = UserC.Grupa;
            forma.PostaviUserControl();
        }

        private void detebtn_Click(object sender, EventArgs e)
        {
            ShowPanel((Button)sender);
            userC = UserC.Dete;
            forma.PostaviUserControl();
        }

        private void klijentbtn_Click(object sender, EventArgs e)
        {
            ShowPanel((Button)sender);
            userC = UserC.Klijent;
            forma.PostaviUserControl();
        }

        private void sobabtn_Click(object sender, EventArgs e)
        {
            ShowPanel((Button)sender);
            userC = UserC.Soba;
            forma.PostaviUserControl();
        }
        public void InsertSoba(bool vrednost)
        {
            sobabtn.Visible = vrednost;
            uslugabtn.Visible = vrednost;
            medtretmanbtn.Visible = vrednost;
            detebtn.Visible = vrednost;
            uslugabtn.Visible = false;
        }
        private void uslugabtn_Click(object sender, EventArgs e)
        {
            ShowPanel((Button)sender);
            forma.PostaviUserControl();
            userC=UserC.Usluga;
        }
    }
}
