﻿namespace Baze3.User_Controls
{
    partial class DodajMedicinskiTretman
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.medBG = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.Klijentbtn = new System.Windows.Forms.Button();
            this.btnDodajTretman = new System.Windows.Forms.Button();
            this.medBG.SuspendLayout();
            this.SuspendLayout();
            // 
            // medBG
            // 
            this.medBG.Controls.Add(this.label3);
            this.medBG.Controls.Add(this.label2);
            this.medBG.Controls.Add(this.textBox2);
            this.medBG.Controls.Add(this.textBox1);
            this.medBG.Location = new System.Drawing.Point(35, 48);
            this.medBG.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.medBG.Name = "medBG";
            this.medBG.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.medBG.Size = new System.Drawing.Size(277, 171);
            this.medBG.TabIndex = 11;
            this.medBG.TabStop = false;
            this.medBG.Text = "Medicinski tretman";
            this.medBG.Enter += new System.EventHandler(this.medBG_Enter);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(47, 93);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Specijalnost";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(47, 54);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Ime lekara";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(134, 89);
            this.textBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(98, 20);
            this.textBox2.TabIndex = 12;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(134, 50);
            this.textBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(98, 20);
            this.textBox1.TabIndex = 11;
            // 
            // Klijentbtn
            // 
            this.Klijentbtn.Location = new System.Drawing.Point(224, 370);
            this.Klijentbtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Klijentbtn.Name = "Klijentbtn";
            this.Klijentbtn.Size = new System.Drawing.Size(88, 37);
            this.Klijentbtn.TabIndex = 12;
            this.Klijentbtn.Text = "Sacuvaj";
            this.Klijentbtn.UseVisualStyleBackColor = true;
            this.Klijentbtn.Click += new System.EventHandler(this.Klijentbtn_Click);
            // 
            // btnDodajTretman
            // 
            this.btnDodajTretman.Location = new System.Drawing.Point(224, 303);
            this.btnDodajTretman.Margin = new System.Windows.Forms.Padding(2);
            this.btnDodajTretman.Name = "btnDodajTretman";
            this.btnDodajTretman.Size = new System.Drawing.Size(88, 37);
            this.btnDodajTretman.TabIndex = 13;
            this.btnDodajTretman.Text = "Dodaj tretman okruzenju";
            this.btnDodajTretman.UseVisualStyleBackColor = true;
            this.btnDodajTretman.Click += new System.EventHandler(this.btnDodajTretman_Click);
            // 
            // DodajMedicinskiTretman
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSalmon;
            this.Controls.Add(this.btnDodajTretman);
            this.Controls.Add(this.Klijentbtn);
            this.Controls.Add(this.medBG);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "DodajMedicinskiTretman";
            this.Size = new System.Drawing.Size(368, 455);
            this.medBG.ResumeLayout(false);
            this.medBG.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox medBG;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button Klijentbtn;
        private System.Windows.Forms.Button btnDodajTretman;
    }
}
