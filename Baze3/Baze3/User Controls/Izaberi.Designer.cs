﻿namespace Baze3
{
    partial class Izaberi
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.klijentbtn = new System.Windows.Forms.Button();
            this.okruzenjebtn = new System.Windows.Forms.Button();
            this.medtretmanbtn = new System.Windows.Forms.Button();
            this.apartmanbtn = new System.Windows.Forms.Button();
            this.hotelbtn = new System.Windows.Forms.Button();
            this.rezervacijabtn = new System.Windows.Forms.Button();
            this.grupabtn = new System.Windows.Forms.Button();
            this.detebtn = new System.Windows.Forms.Button();
            this.sobabtn = new System.Windows.Forms.Button();
            this.uslugabtn = new System.Windows.Forms.Button();
            this.SidePanel = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // klijentbtn
            // 
            this.klijentbtn.FlatAppearance.BorderSize = 0;
            this.klijentbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.klijentbtn.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.klijentbtn.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.klijentbtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.klijentbtn.Location = new System.Drawing.Point(15, 227);
            this.klijentbtn.Margin = new System.Windows.Forms.Padding(2);
            this.klijentbtn.Name = "klijentbtn";
            this.klijentbtn.Size = new System.Drawing.Size(153, 45);
            this.klijentbtn.TabIndex = 9;
            this.klijentbtn.Text = "Klijent";
            this.klijentbtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.klijentbtn.UseVisualStyleBackColor = true;
            this.klijentbtn.Click += new System.EventHandler(this.klijentbtn_Click);
            // 
            // okruzenjebtn
            // 
            this.okruzenjebtn.FlatAppearance.BorderSize = 0;
            this.okruzenjebtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.okruzenjebtn.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.okruzenjebtn.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.okruzenjebtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.okruzenjebtn.Location = new System.Drawing.Point(15, 104);
            this.okruzenjebtn.Margin = new System.Windows.Forms.Padding(2);
            this.okruzenjebtn.Name = "okruzenjebtn";
            this.okruzenjebtn.Size = new System.Drawing.Size(153, 37);
            this.okruzenjebtn.TabIndex = 10;
            this.okruzenjebtn.Text = "Okruzenje";
            this.okruzenjebtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.okruzenjebtn.UseVisualStyleBackColor = true;
            this.okruzenjebtn.Click += new System.EventHandler(this.okruzenjebtn_Click);
            // 
            // medtretmanbtn
            // 
            this.medtretmanbtn.FlatAppearance.BorderSize = 0;
            this.medtretmanbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.medtretmanbtn.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.medtretmanbtn.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.medtretmanbtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.medtretmanbtn.Location = new System.Drawing.Point(15, 276);
            this.medtretmanbtn.Margin = new System.Windows.Forms.Padding(2);
            this.medtretmanbtn.Name = "medtretmanbtn";
            this.medtretmanbtn.Size = new System.Drawing.Size(153, 49);
            this.medtretmanbtn.TabIndex = 11;
            this.medtretmanbtn.Text = "Medicinski tretman";
            this.medtretmanbtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.medtretmanbtn.UseVisualStyleBackColor = true;
            this.medtretmanbtn.Click += new System.EventHandler(this.medtretmanbtn_Click);
            // 
            // apartmanbtn
            // 
            this.apartmanbtn.FlatAppearance.BorderSize = 0;
            this.apartmanbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.apartmanbtn.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.apartmanbtn.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.apartmanbtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.apartmanbtn.Location = new System.Drawing.Point(15, 56);
            this.apartmanbtn.Margin = new System.Windows.Forms.Padding(2);
            this.apartmanbtn.Name = "apartmanbtn";
            this.apartmanbtn.Size = new System.Drawing.Size(153, 44);
            this.apartmanbtn.TabIndex = 12;
            this.apartmanbtn.Text = "Apartman";
            this.apartmanbtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.apartmanbtn.UseVisualStyleBackColor = true;
            this.apartmanbtn.Click += new System.EventHandler(this.apartmanbtn_Click);
            // 
            // hotelbtn
            // 
            this.hotelbtn.FlatAppearance.BorderSize = 0;
            this.hotelbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.hotelbtn.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hotelbtn.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.hotelbtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.hotelbtn.Location = new System.Drawing.Point(15, 15);
            this.hotelbtn.Margin = new System.Windows.Forms.Padding(2);
            this.hotelbtn.Name = "hotelbtn";
            this.hotelbtn.Size = new System.Drawing.Size(153, 37);
            this.hotelbtn.TabIndex = 13;
            this.hotelbtn.Text = "Hotel";
            this.hotelbtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.hotelbtn.UseVisualStyleBackColor = true;
            this.hotelbtn.Click += new System.EventHandler(this.button4_Click);
            // 
            // rezervacijabtn
            // 
            this.rezervacijabtn.FlatAppearance.BorderSize = 0;
            this.rezervacijabtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rezervacijabtn.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rezervacijabtn.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.rezervacijabtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rezervacijabtn.Location = new System.Drawing.Point(15, 145);
            this.rezervacijabtn.Margin = new System.Windows.Forms.Padding(2);
            this.rezervacijabtn.Name = "rezervacijabtn";
            this.rezervacijabtn.Size = new System.Drawing.Size(153, 37);
            this.rezervacijabtn.TabIndex = 14;
            this.rezervacijabtn.Text = "Rezervacija";
            this.rezervacijabtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.rezervacijabtn.UseVisualStyleBackColor = true;
            this.rezervacijabtn.Click += new System.EventHandler(this.rezervacijabtn_Click);
            // 
            // grupabtn
            // 
            this.grupabtn.FlatAppearance.BorderSize = 0;
            this.grupabtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grupabtn.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grupabtn.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.grupabtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.grupabtn.Location = new System.Drawing.Point(15, 186);
            this.grupabtn.Margin = new System.Windows.Forms.Padding(2);
            this.grupabtn.Name = "grupabtn";
            this.grupabtn.Size = new System.Drawing.Size(153, 37);
            this.grupabtn.TabIndex = 15;
            this.grupabtn.Text = "Grupa";
            this.grupabtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.grupabtn.UseVisualStyleBackColor = true;
            this.grupabtn.Click += new System.EventHandler(this.grupabtn_Click);
            // 
            // detebtn
            // 
            this.detebtn.FlatAppearance.BorderSize = 0;
            this.detebtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.detebtn.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.detebtn.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.detebtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.detebtn.Location = new System.Drawing.Point(15, 329);
            this.detebtn.Margin = new System.Windows.Forms.Padding(2);
            this.detebtn.Name = "detebtn";
            this.detebtn.Size = new System.Drawing.Size(153, 37);
            this.detebtn.TabIndex = 16;
            this.detebtn.Text = "Dete";
            this.detebtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.detebtn.UseVisualStyleBackColor = true;
            this.detebtn.Click += new System.EventHandler(this.detebtn_Click);
            // 
            // sobabtn
            // 
            this.sobabtn.FlatAppearance.BorderSize = 0;
            this.sobabtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sobabtn.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sobabtn.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.sobabtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.sobabtn.Location = new System.Drawing.Point(15, 370);
            this.sobabtn.Margin = new System.Windows.Forms.Padding(2);
            this.sobabtn.Name = "sobabtn";
            this.sobabtn.Size = new System.Drawing.Size(153, 44);
            this.sobabtn.TabIndex = 17;
            this.sobabtn.Text = "Soba";
            this.sobabtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.sobabtn.UseVisualStyleBackColor = true;
            this.sobabtn.Click += new System.EventHandler(this.sobabtn_Click);
            // 
            // uslugabtn
            // 
            this.uslugabtn.FlatAppearance.BorderSize = 0;
            this.uslugabtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.uslugabtn.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uslugabtn.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.uslugabtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uslugabtn.Location = new System.Drawing.Point(15, 418);
            this.uslugabtn.Margin = new System.Windows.Forms.Padding(2);
            this.uslugabtn.Name = "uslugabtn";
            this.uslugabtn.Size = new System.Drawing.Size(153, 37);
            this.uslugabtn.TabIndex = 18;
            this.uslugabtn.Text = "Usluga";
            this.uslugabtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.uslugabtn.UseVisualStyleBackColor = true;
            this.uslugabtn.Click += new System.EventHandler(this.uslugabtn_Click);
            // 
            // SidePanel
            // 
            this.SidePanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(8)))), ((int)(((byte)(55)))));
            this.SidePanel.Location = new System.Drawing.Point(2, 2);
            this.SidePanel.Margin = new System.Windows.Forms.Padding(2);
            this.SidePanel.Name = "SidePanel";
            this.SidePanel.Size = new System.Drawing.Size(8, 41);
            this.SidePanel.TabIndex = 19;
            // 
            // Izaberi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.Controls.Add(this.SidePanel);
            this.Controls.Add(this.uslugabtn);
            this.Controls.Add(this.sobabtn);
            this.Controls.Add(this.detebtn);
            this.Controls.Add(this.grupabtn);
            this.Controls.Add(this.rezervacijabtn);
            this.Controls.Add(this.hotelbtn);
            this.Controls.Add(this.apartmanbtn);
            this.Controls.Add(this.medtretmanbtn);
            this.Controls.Add(this.okruzenjebtn);
            this.Controls.Add(this.klijentbtn);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Izaberi";
            this.Size = new System.Drawing.Size(180, 457);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button klijentbtn;
        private System.Windows.Forms.Button okruzenjebtn;
        private System.Windows.Forms.Button medtretmanbtn;
        private System.Windows.Forms.Button apartmanbtn;
        private System.Windows.Forms.Button hotelbtn;
        private System.Windows.Forms.Button rezervacijabtn;
        private System.Windows.Forms.Button grupabtn;
        private System.Windows.Forms.Button detebtn;
        private System.Windows.Forms.Button sobabtn;
        private System.Windows.Forms.Button uslugabtn;
        private System.Windows.Forms.Panel SidePanel;
    }
}
