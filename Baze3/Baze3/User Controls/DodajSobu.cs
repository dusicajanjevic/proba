﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NHibernate;
using Baze3.Entiteti;

namespace Baze3.User_Controls
{
    public partial class DodajSobu : UserControl
    {
        public Operacija operacija { get; set; }
        public DodajHotel Hotel { get; set; }
        public Entiteti.Hotel pripadaHotelu;
        Provajderi.SobaProvajder sobaProvajder;
        public int SobaId;
        public DodajSobu()
        {
            InitializeComponent();
            sobaProvajder = new Provajderi.SobaProvajder();
        }
      
        private void Klijentbtn_Click(object sender, EventArgs e)
        {
            Entiteti.Soba soba;
            if (radioButton2.Checked)
            {
                soba = new Entiteti.Jednokrevetna();
                soba.Tip = 1;
            }
            else
                if (radioButton1.Checked)
            {
                soba = new Entiteti.Dvokrevetna();
                soba.Tip = 2;
            }
            else
            {
                soba = new Entiteti.Trokrevetna();
                soba.Tip = 3;
            }

            soba.Broj = int.Parse(textBox1.Text);
            soba.Sprat = int.Parse(textBox2.Text);
            soba.Tv = "NE";
            soba.More = "NE";
            soba.Radio = "NE";
            soba.Kupatilo = "NE";
            soba.Klima = "NE";
            soba.Slobodno = "NE";
            soba.Frizider = "NE";
            
            foreach (object Item in checkedListBox1.CheckedItems)
            {
                switch (Item)
                {
                    case ("TV"): { soba.Tv = "DA"; } break;
                    case ("Pogled na more"): { soba.More = "DA"; } break;
                    case ("Radio"): { soba.Radio = "DA"; } break;
                    case ("Kupatilo"): { soba.Kupatilo = "DA"; } break;
                    case ("Klima"): { soba.Klima = "DA"; } break;
                    case ("Slobodna"): { soba.Slobodno = "DA"; } break;
                    case ("Frizider"): { soba.Frizider = "DA"; } break;
                }
            }

            Provajderi.UslugaProvider up = new Provajderi.UslugaProvider();

            soba.PripadaHotelu = pripadaHotelu;
            if (sobaProvajder.AddSoba(soba) == 1)
                MessageBox.Show("Soba je snimljena.");
            else
                MessageBox.Show("Neuspesno snimljena soba!");

            List<Usluga> usluge = new List<Entiteti.Usluga>();

            if (chbSpavanje.Checked)
            {
                try
                {
                    ISession s = DataLayer.GetSession();

                    Entiteti.Usluga u = up.GetUsluga(21);

                    Entiteti.VezanaJe vezana_je = new Entiteti.VezanaJe();
                    vezana_je.Soba = soba;
                    vezana_je.Usluga = u;
                    vezana_je.Cena = int.Parse(txtSpavanjeCena.Text);

                    s.Save(vezana_je);

                    s.Flush();
                    s.Close();

                    MessageBox.Show("Uspesno vezana soba i cena.");
                    usluge.Add(u);
                }
                catch (Exception exc)
                {
                    MessageBox.Show("Greska VezanaJe: " + exc);
                }
            }
            if (chbPolupansion.Checked)
            {
                try
                {
                    ISession s = DataLayer.GetSession();

                    Entiteti.Usluga u = up.GetUsluga(22);

                    Entiteti.VezanaJe vezana_je = new Entiteti.VezanaJe();
                    vezana_je.Soba = soba;
                    vezana_je.Usluga = u;
                    vezana_je.Cena = int.Parse(txtPopupansionCena.Text);

                    s.Save(vezana_je);

                    s.Flush();
                    s.Close();

                    MessageBox.Show("Uspesno vezana soba i cena.");
                    usluge.Add(u);
                }
                catch (Exception exc)
                {
                    MessageBox.Show("Greska VezanaJe: " + exc);
                }
            }
            if (chbPansion.Checked)
            {
                try
                {
                    ISession s = DataLayer.GetSession();

                    Entiteti.Usluga u = up.GetUsluga(23);

                    Entiteti.VezanaJe vezana_je = new Entiteti.VezanaJe();
                    vezana_je.Soba = soba;
                    vezana_je.Usluga = u;
                    vezana_je.Cena = int.Parse(txtPansionCena.Text);

                    s.Save(vezana_je);

                    s.Flush();
                    s.Close();

                    MessageBox.Show("Uspesno vezana soba i cena.");
                    usluge.Add(u);
                }
                catch (Exception exc)
                {
                    MessageBox.Show("Greska VezanaJe: " + exc);
                }
            }
            
            foreach (Usluga u in usluge)
            {
                pripadaHotelu.ImaUsluge.Add(u);
                u.ImaHotele.Add(pripadaHotelu);
                //up.UpdateUsluga(u.Id, u);
            }

            Provajderi.HotelProvajder hp = new Provajderi.HotelProvajder();
            hp.UpdateHotel(pripadaHotelu.IdSmestaja, pripadaHotelu);
            
            if (operacija == Operacija.Update)
            {

                if (sobaProvajder.UpdateSoba(SobaId, soba))
                    MessageBox.Show("Uspesan update.");
                else
                    MessageBox.Show("Neuspesan update!");
            }
            else
            {
                Hotel.FormaZaSobu = this;
                Hotel.DodajSobuHotelu(soba);
                MessageBox.Show("Soba je dodata hotelu.");
                Hotel.BringToFront();
            }
        }
    }
}