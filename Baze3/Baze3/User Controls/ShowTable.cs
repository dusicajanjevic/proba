﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Baze3.Provajderi;
using Baze3.Entiteti;

namespace Baze3.User_Controls
{
    public partial class ShowTable : UserControl
    {
        
        HotelProvajder hotelProvajder;
        ApartmanProvajder apartmanProvajder;
        KlijentProvajder klijentProvajder;
        DeteProvajder deteProvajder;
        OkruzenjeProvajder okruzenjeProvajder;
        SobaProvajder sobaProvajder;
        GrupaProvajder grupaProvajder;
        MedicinskiTretmanProvajder medicinskiP;
        RezervacijaProvajder rezervacijaProvajder;
        UslugaProvider uslugaProvajder;

        public DodajHotel dodajHotel { get; set; }
        public DodajApartman dodajApartman { get; set; }
        public DodajOkruzenje dodajOkruzenje { get; set; }
        public DodajSobu dodajSobu { get; set; }
        public DodajMedicinskiTretman dodajTretman { get; set; }
        public DodajDete dodajDete { get; set; }
        public DodajKlijenta dodajKlijenta { get; set; }
        public DodajRezervaciju dodajRezervaciju { get; set; }
        public DodajGrupu dodajGrupu { get; set; }

        public UserC tipOperacije { get; set; }

        public char tip;

        public ShowTable()
        {
            InitializeComponent();
            rezervacijaProvajder = new RezervacijaProvajder();
            okruzenjeProvajder = new OkruzenjeProvajder();
            hotelProvajder = new HotelProvajder();
            sobaProvajder = new SobaProvajder();
            grupaProvajder = new GrupaProvajder();
            medicinskiP = new MedicinskiTretmanProvajder();
            deteProvajder = new DeteProvajder();
            klijentProvajder = new KlijentProvajder();
            apartmanProvajder = new ApartmanProvajder();
        }
        public void PrikazHotela()
        {
            List<Hotel> list = new List<Hotel>();
            
            list = hotelProvajder.Lista();
            BindingList<Hotel> bindingList = new BindingList<Hotel>(list);
                dgv.DataSource = bindingList;
            tipOperacije = UserC.Hotel;
        }

        public void PrikazApartmana()
        {          
            List<Apartman> list = new List<Apartman>();
            BindingList<Apartman> bindingList;
            list = apartmanProvajder.Lista();
            bindingList = new BindingList<Apartman>(list);
            dgv.DataSource = bindingList;
            tipOperacije = UserC.Apartman;
        }
        public void PrikazKlijenta()
        {
            List<Klijent> list = new List<Klijent>();
            BindingList<Klijent> bindingList;
            list = klijentProvajder.Lista();
            bindingList = new BindingList<Klijent>(list);
            dgv.DataSource = bindingList;
            tipOperacije = UserC.Klijent;
        }
        public void PrikazDece()
        {
            List<Dete> list = new List<Dete>();
            BindingList<Dete> bindingList;
            list = deteProvajder.Lista();
            bindingList = new BindingList<Dete>(list);
            dgv.DataSource = bindingList;
            tipOperacije = UserC.Dete;
        }
        public void PrikazOkruzenja()
        {
            List<Okruzenje> list = new List<Okruzenje>();
            BindingList<Okruzenje> bindingList;
            list = okruzenjeProvajder.Lista();
            bindingList = new BindingList<Okruzenje>(list);
            dgv.DataSource = bindingList;
            tipOperacije = UserC.Okruzenje;
        }
        public void Brisem()
        {
            button1.Visible = true;
            updatebtn.Visible = false;
            btnKlijent.Visible = false;
        }
        public void Prikaz()
        {
            button1.Visible = false;
            updatebtn.Visible = false;
            btnKlijent.Visible = false;
        }
        public void Odabir()
        {
            button1.Visible = false;
            updatebtn.Visible = false;
            btnKlijent.Visible = true;
        }
        public void Izmeni()
        {
            button1.Visible = false;
            updatebtn.Visible = true;
            btnKlijent.Visible = false;
        }
        public void PrikazMedTretmana()
        {
            List<MedicinskiTretman> list = new List<MedicinskiTretman>();
            BindingList<MedicinskiTretman> bindingList;
            list = medicinskiP.Lista();
            bindingList = new BindingList<MedicinskiTretman>(list);
            dgv.DataSource = bindingList;
            tipOperacije = UserC.MedTretman; 
        }
        public void PrikazGrupe()
        {
            List<Grupa> list = new List<Grupa>();
            BindingList<Grupa> bindingList;
            list = grupaProvajder.Lista();
            bindingList = new BindingList<Grupa>(list);
            dgv.DataSource = bindingList;
            tipOperacije = UserC.Grupa;
        }
        public void PrikazRezervacije()
        {
            List<Rezervacija> list = new List<Rezervacija>();
            BindingList<Rezervacija> bindingList;
            list = rezervacijaProvajder.Lista();
            bindingList = new BindingList<Rezervacija>(list);
            dgv.DataSource = bindingList;
            tipOperacije = UserC.Rezervacija;
        }
        public void PrikazSoba()
        {
            List<Soba> list = new List<Soba>();
            BindingList<Soba> bindingList;
            list = sobaProvajder.Lista();
            bindingList = new BindingList<Soba>(list);
            dgv.DataSource = bindingList;
            tipOperacije = UserC.Soba;
        }
        public void PrikazUsluga()
        {
            List<Usluga> list = new List<Usluga>();
            BindingList<Usluga> bindingList;
            list = uslugaProvajder.Lista();
            bindingList = new BindingList<Usluga>(list);
            dgv.DataSource = bindingList;
            tipOperacije = UserC.Usluga;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            switch (tipOperacije)
            {
                case (UserC.Soba):
                    {
                        List<Soba> list;
                        list = sobaProvajder.Lista();
                        int broj = int.Parse(dgv.SelectedRows[0].Cells["Id"].Value.ToString());
                        BindingList<Soba> bindingList = new BindingList<Soba>(list);
                        if(sobaProvajder.RemoveSoba(broj) == 1)
                        {
                            MessageBox.Show("Obrisano");
                            bindingList.RemoveAt(dgv.SelectedRows[0].Index);
                        }
                        else
                            MessageBox.Show("Greska pri brisanju");
                        dgv.Refresh();

                    } break;
                case (UserC.Hotel):
                    {
                        List<Hotel> list;
                        list = hotelProvajder.Lista();
                        int broj = int.Parse(dgv.SelectedRows[0].Cells["IdSmestaja"].Value.ToString());
                        BindingList<Hotel> bindingList = new BindingList<Hotel>(list);
                        if(hotelProvajder.RemoveHotel(broj) == 1)
                        {
                            MessageBox.Show("Obrisano");
                            bindingList.RemoveAt(dgv.SelectedRows[0].Index);
                        }
                        else
                            MessageBox.Show("Greska pri brisanju");
                        dgv.Refresh();
                    } break;
                case (UserC.Okruzenje): {
                        List<Okruzenje> list;
                        list = okruzenjeProvajder.Lista();
                        int broj = int.Parse(dgv.SelectedRows[0].Cells["Id"].Value.ToString());
                        BindingList<Okruzenje> bindingList = new BindingList<Okruzenje>(list);
                        if (okruzenjeProvajder.RemoveOkruzenje(broj) == 1)
                        {
                            MessageBox.Show("Obrisano");
                            bindingList.RemoveAt(dgv.SelectedRows[0].Index);
                        }
                        else
                            MessageBox.Show("Greska pri brisanju");
                        dgv.Refresh();
                    } break;
                case (UserC.MedTretman): {
                        List<MedicinskiTretman> list;
                        list = medicinskiP.Lista();
                           
                        int broj = int.Parse(dgv.SelectedRows[0].Cells["Id"].Value.ToString());
                        BindingList<MedicinskiTretman> bindingList = new BindingList<MedicinskiTretman>(list);
                        if(medicinskiP.RemoveMedicinskiTretman(broj) == 1)
                        {
                            MessageBox.Show("Obrisano");
                            bindingList.RemoveAt(dgv.SelectedRows[0].Index);
                        }
                        else
                            MessageBox.Show("Greska pri brisanju");
                        dgv.Refresh();
                    } break;
                case (UserC.Dete): {
                        List<Dete> list;
                        list = deteProvajder.Lista();
                        int broj = int.Parse(dgv.SelectedRows[0].Cells["Id"].Value.ToString());
                        BindingList<Dete> bindingList = new BindingList<Dete>(list);
                        if(deteProvajder.RemoveDete(broj) == 1)
                        {
                            MessageBox.Show("Obrisano");
                            bindingList.RemoveAt(dgv.SelectedRows[0].Index);
                        }
                        else
                            MessageBox.Show("Greska pri brisanju");
                        dgv.Refresh();
                    } break;
                case (UserC.Klijent): {
                        List<Klijent> list;
                        list = klijentProvajder.Lista();
                        int broj = int.Parse(dgv.SelectedRows[0].Cells["Id"].Value.ToString());
                        BindingList<Klijent> bindingList = new BindingList<Klijent>(list);
                        if(klijentProvajder.RemoveKlijent(broj) == 1)
                        {
                            MessageBox.Show("Obrisano");
                            bindingList.RemoveAt(dgv.SelectedRows[0].Index);
                        }
                        else
                            MessageBox.Show("Greska pri brisanju");
                        dgv.Refresh();
                    } break;
                case (UserC.Apartman): {
                        List<Apartman> list = new List<Apartman>();
                        list = apartmanProvajder.Lista();
                        int broj = int.Parse(dgv.SelectedRows[0].Cells["IdSmestaja"].Value.ToString());
                        BindingList<Apartman> bindingList = new BindingList<Apartman>(list);
                        if(apartmanProvajder.RemoveApartman(broj) == 1)
                        {
                            MessageBox.Show("Obrisano");
                            bindingList.RemoveAt(dgv.SelectedRows[0].Index);
                        }
                        else
                            MessageBox.Show("Greska pri brisanju");
                        dgv.Refresh();
                    } break;
                case (UserC.Rezervacija): {
                        List<Rezervacija> list;
                        list = rezervacijaProvajder.Lista();
                        int broj = int.Parse(dgv.SelectedRows[0].Cells["Id"].Value.ToString());
                        BindingList<Rezervacija> bindingList = new BindingList<Rezervacija>(list);
                        if(rezervacijaProvajder.RemoveRezervacija(broj) == 1)
                        {
                            MessageBox.Show("Obrisano");
                            bindingList.RemoveAt(dgv.SelectedRows[0].Index);
                        }
                        else
                            MessageBox.Show("Greska pri brisanju");
                        dgv.Refresh();
                    } break;
                case (UserC.Grupa): {
                        List<Grupa> list;
                        list = grupaProvajder.Lista();
                        int broj = int.Parse(dgv.SelectedRows[0].Cells["Id"].Value.ToString());
                        BindingList<Grupa> bindingList = new BindingList<Grupa>(list);
                        if (grupaProvajder.RemoveGrupa(broj) == 1)
                        {
                            MessageBox.Show("Obrisano");
                            bindingList.RemoveAt(dgv.SelectedRows[0].Index);
                        }
                        else
                            MessageBox.Show("Greska pri brisanju");
                        dgv.Refresh();
                    } break;
                case (UserC.Usluga):
                    {
                        List<Usluga> list;
                        list = uslugaProvajder.Lista();
                        int broj = int.Parse(dgv.SelectedRows[0].Cells["Id"].Value.ToString());
                        BindingList<Usluga> bindingList = new BindingList<Usluga>(list);
                        if (uslugaProvajder.RemoveUsluga(broj) == 1)
                        {
                            MessageBox.Show("Obrisano");
                            bindingList.RemoveAt(dgv.SelectedRows[0].Index);
                        }
                        else
                            MessageBox.Show("Greska pri brisanju");
                        dgv.Refresh();
                    }
                    break;
            }
        
        }

        private void updatebtn_Click(object sender, EventArgs e)
        {
            switch (tipOperacije)
            {
                case (UserC.Soba):
                    {
                        dodajSobu.SobaId = int.Parse(dgv.SelectedRows[0].Cells["Id"].Value.ToString());
                        dodajSobu.Visible = true;
                        dodajSobu.BringToFront();
                        dodajSobu.operacija = Operacija.Update;
                    }
                    break;
                case (UserC.Hotel):
                    {
                        dodajHotel.HotelId = int.Parse(dgv.SelectedRows[0].Cells["IdSmestaja"].Value.ToString());
                        dodajHotel.Visible = true;
                        dodajHotel.BringToFront();
                        dodajHotel.operacija = Operacija.Update;
                    }
                    break;
                case (UserC.Okruzenje):
                    {
                        dodajOkruzenje.OkruzenjeId = int.Parse(dgv.SelectedRows[0].Cells["Id"].Value.ToString());
                        dodajOkruzenje.Visible = true;
                        dodajOkruzenje.BringToFront();
                        dodajOkruzenje.operacija = Operacija.Update;
                    }
                    break;
                case (UserC.MedTretman):
                    {
                        dodajTretman.TretmanId = int.Parse(dgv.SelectedRows[0].Cells["Id"].Value.ToString());
                        dodajTretman.Visible = true;
                        dodajTretman.BringToFront();
                        dodajTretman.operacija = Operacija.Update;
                    }
                    break;
                case (UserC.Dete):
                    {
                        dodajDete.DeteId = int.Parse(dgv.SelectedRows[0].Cells["Id"].Value.ToString());
                        dodajDete.Visible = true;
                        dodajDete.BringToFront();
                        dodajDete.operacija = Operacija.Update;
                    }
                    break;
                case (UserC.Klijent):
                    {
                        dodajKlijenta.KlijentId = int.Parse(dgv.SelectedRows[0].Cells["Id"].Value.ToString());
                        dodajKlijenta.Visible = true;
                        dodajKlijenta.BringToFront();
                        dodajKlijenta.operacija = Operacija.Update;
                    }
                    break;
                case (UserC.Apartman):
                    {
                        dodajApartman.ApartmanId = int.Parse(dgv.SelectedRows[0].Cells["Id"].Value.ToString());
                        dodajApartman.Visible = true;
                        dodajApartman.BringToFront();
                        dodajApartman.operacija = Operacija.Update;
                    }
                    break;
                case (UserC.Rezervacija):
                    {
                        dodajRezervaciju.RezervacijaId = int.Parse(dgv.SelectedRows[0].Cells["Id"].Value.ToString());
                        dodajRezervaciju.Visible = true;
                        dodajRezervaciju.BringToFront();
                        dodajRezervaciju.operacija = Operacija.Update;
                    }
                    break;
                case (UserC.Grupa):
                    {
                        dodajGrupu.GrupaId = int.Parse(dgv.SelectedRows[0].Cells["Id"].Value.ToString());
                        dodajGrupu.Visible = true;
                        dodajGrupu.BringToFront();
                        dodajGrupu.operacija = Operacija.Update;
                    }
                    break;
            }
        }

        private void btnKlijent_Click(object sender, EventArgs e)
        {
            if (tip == 'k')
            {
                int idK = int.Parse(dgv.SelectedRows[0].Cells["Id"].Value.ToString());
                dodajRezervaciju.Visible = true;
                dodajRezervaciju.BringToFront();
                dodajRezervaciju.klijentRez = klijentProvajder.GetKlijent(idK);
            }
            if(tip=='h')
            {
                int idH = int.Parse(dgv.SelectedRows[0].Cells["Id"].Value.ToString());
                dodajRezervaciju.Visible = true;
                dodajRezervaciju.BringToFront();
                dodajRezervaciju.sobaRez = sobaProvajder.GetSoba(idH);
            }
            if (tip == 'a')
            {
                int idA = int.Parse(dgv.SelectedRows[0].Cells["IdSmestaja"].Value.ToString());
                dodajRezervaciju.Visible = true;
                dodajRezervaciju.BringToFront();
                dodajRezervaciju.apartmanRez = apartmanProvajder.GetApartman(idA);
            }
        }
    }
}
