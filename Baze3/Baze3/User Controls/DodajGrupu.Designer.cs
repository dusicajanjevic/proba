﻿namespace Baze3.User_Controls
{
    partial class DodajGrupu
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.unesibtn = new System.Windows.Forms.Button();
            this.grupaGB = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.nazivtxt = new System.Windows.Forms.TextBox();
            this.btnPridruzi = new System.Windows.Forms.Button();
            this.grupaGB.SuspendLayout();
            this.SuspendLayout();
            // 
            // unesibtn
            // 
            this.unesibtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(8)))), ((int)(((byte)(55)))));
            this.unesibtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.unesibtn.Location = new System.Drawing.Point(171, 246);
            this.unesibtn.Margin = new System.Windows.Forms.Padding(2);
            this.unesibtn.Name = "unesibtn";
            this.unesibtn.Size = new System.Drawing.Size(139, 37);
            this.unesibtn.TabIndex = 4;
            this.unesibtn.Text = "Sacuvaj";
            this.unesibtn.UseVisualStyleBackColor = false;
            this.unesibtn.Click += new System.EventHandler(this.Klijentbtn_Click);
            // 
            // grupaGB
            // 
            this.grupaGB.Controls.Add(this.label6);
            this.grupaGB.Controls.Add(this.nazivtxt);
            this.grupaGB.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grupaGB.Location = new System.Drawing.Point(21, 72);
            this.grupaGB.Margin = new System.Windows.Forms.Padding(2);
            this.grupaGB.Name = "grupaGB";
            this.grupaGB.Padding = new System.Windows.Forms.Padding(2);
            this.grupaGB.Size = new System.Drawing.Size(318, 148);
            this.grupaGB.TabIndex = 3;
            this.grupaGB.TabStop = false;
            this.grupaGB.Text = "Grupa";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(35, 37);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 17);
            this.label6.TabIndex = 2;
            this.label6.Text = "Naziv grupe";
            // 
            // nazivtxt
            // 
            this.nazivtxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nazivtxt.Location = new System.Drawing.Point(150, 37);
            this.nazivtxt.Margin = new System.Windows.Forms.Padding(2);
            this.nazivtxt.Name = "nazivtxt";
            this.nazivtxt.Size = new System.Drawing.Size(140, 23);
            this.nazivtxt.TabIndex = 1;
            // 
            // btnPridruzi
            // 
            this.btnPridruzi.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(8)))), ((int)(((byte)(55)))));
            this.btnPridruzi.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPridruzi.Location = new System.Drawing.Point(172, 303);
            this.btnPridruzi.Margin = new System.Windows.Forms.Padding(2);
            this.btnPridruzi.Name = "btnPridruzi";
            this.btnPridruzi.Size = new System.Drawing.Size(139, 50);
            this.btnPridruzi.TabIndex = 5;
            this.btnPridruzi.Text = "Pridruzi klijenta grupi";
            this.btnPridruzi.UseVisualStyleBackColor = false;
            this.btnPridruzi.Click += new System.EventHandler(this.btnPridruzi_Click);
            // 
            // DodajGrupu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.RosyBrown;
            this.Controls.Add(this.btnPridruzi);
            this.Controls.Add(this.unesibtn);
            this.Controls.Add(this.grupaGB);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "DodajGrupu";
            this.Size = new System.Drawing.Size(368, 455);
            this.grupaGB.ResumeLayout(false);
            this.grupaGB.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button unesibtn;
        private System.Windows.Forms.GroupBox grupaGB;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox nazivtxt;
        private System.Windows.Forms.Button btnPridruzi;
    }
}
