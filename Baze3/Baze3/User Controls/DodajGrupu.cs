﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Baze3.Provajderi;
using Baze3.Entiteti;
using NHibernate;

namespace Baze3.User_Controls
{
    public partial class DodajGrupu : UserControl
    {
        GrupaProvajder provajder;
        public int GrupaId;
        public Operacija operacija { get; set; }
        List<Entiteti.Klijent> klijenti;
        Grupa g;
        public Klijent KlijentKojiSePridruzuje;
        public DodajKlijenta dodajKlijenta { get; set; }

        public DodajGrupu()
        {
            InitializeComponent();
            provajder = new GrupaProvajder();
            klijenti = new List<Klijent>();
        }

        public void Prikaz(Operacija o)
        { 
            if (o == Operacija.Insert)
            {
                unesibtn.Visible = true;
              
            }
            else if (o == Operacija.Update)
            {
                unesibtn.Visible = false;           
            }
        }

        public void DisableSacuvaj()
        {
            unesibtn.Visible = false;
        }
        public void EnableSacuvaj()
        {
            unesibtn.Visible = true;
        }
        public void DisableDodajKlijenta()
        {
            btnPridruzi.Visible = false;
        }
        public void EnableDodajKlijenta()
        {
            btnPridruzi.Visible = true;
        }

        private void NapraviGrupu()
        {
            g = new Grupa()
            {
                Naziv = nazivtxt.Text
            };
        }
        private void Klijentbtn_Click(object sender, EventArgs e)
        {
            NapraviGrupu();
            if(provajder.AddGrupa(g)==1)
                MessageBox.Show("Uspesan insert grupe.");
            else
                MessageBox.Show("Neuspesan insert grupe!");
        }

        private void btnPridruzi_Click(object sender, EventArgs e)
        {
            GrupaProvajder gp = new GrupaProvajder();
            Grupa grupa = gp.VratiGrupuPoNazivu(nazivtxt.Text);

            if(grupa==null)
            {
                MessageBox.Show("Ne postoji grupa sa datim nazivom!");
                return;
            }

            dodajKlijenta.pripadaGrupi = grupa;

            dodajKlijenta.BringToFront();
            unesibtn.Visible = true;
        }
    }
}
