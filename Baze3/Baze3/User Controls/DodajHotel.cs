﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Baze3.Entiteti;
using Baze3.Provajderi;
using NHibernate;

namespace Baze3
{
    public partial class DodajHotel : UserControl
    {
        Hotel h;
        List<String> sportskeAkt;
        public User_Controls.DodajSobu FormaZaSobu { get; set; }
        private List<Soba> sobe;
        List<Usluga> usluge;
        public DodajOkruzenje okruzenje { get; set; }

        HotelProvajder hotelProvajder;
        SportskeAktivnostiProvajder sportProvajder;
        public int HotelId { get; set; }

        public Operacija operacija { get; set; }

        public DodajHotel()
        {
            InitializeComponent();
            sportskeAkt = new List<string>();
            hotelProvajder = new HotelProvajder();
            sportProvajder = new SportskeAktivnostiProvajder();
            sobe = new List<Soba>();
            usluge = new List<Usluga>();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            sportskeAkt.Add(sporttxt.Text);
            sporttxt.Clear();
        }
      
        public void HotelUpdate()
        {
            sportbtn.Visible = false;
            sporttxt.Visible = false;
        }

        private void NapraviHotel()
        {
            HotelProvajder hp = new HotelProvajder();

            h = new Hotel()
            {
                Ime = imeHoteltxt.Text,
                Sifra = sifraHoteltxt.Text,
                Adresa = adresaHoteltxt.Text,
                Grad = gradHoteltxt.Text,
                Kategorija = kategorijaHoteltxt.Text,
                Br_lezaja = int.Parse(brLezajaHoteltxt.Text),
                Direktor = dirHoteltxt.Text,
                Restoran_mesta = int.Parse(restoranHoteltxt.Text)
            };
            hp.AddHotel(h);
        }

        private void Sacuvaj()
        {
            SobaProvajder sp = new SobaProvajder();
            UslugaProvider up = new UslugaProvider();

            NapraviHotel();
            switch (operacija)
            {

                case Operacija.Insert:
                    {
                        hotelProvajder.AddHotel(h);

                        foreach (String aktivnost in sportskeAkt)
                        {
                            SportskeAktivnosti s = new SportskeAktivnosti()
                            {
                                Aktivnost = aktivnost,
                                PripadaHotelu = h
                            };
                            if (sportProvajder.AddSportskeAktivnosti(s) == 1)
                            {
                                MessageBox.Show("Uspesan insert sportske aktivnosti.");
                            }
                        }
                        foreach (Soba soba in sobe)
                        {
                            soba.PripadaHotelu = h;
                            sp.UpdateSoba(soba.Id, soba);

                            h.ImaSobe.Add(soba);
                        }

                        if (hotelProvajder.UpdateHotel(h.IdSmestaja, h))
                            MessageBox.Show("Uspesan insert hotela.");
                        else
                            MessageBox.Show("Neuspesan insert hotela!");
                    }
                    break;
                case Operacija.Update:
                    {
                        if (hotelProvajder.UpdateHotel(HotelId, h))
                            MessageBox.Show("Uspesan update.");
                        else
                            MessageBox.Show("Neuspesan update!");
                    }
                    break;

            }

            foreach (TextBox txt in this.Controls.OfType<TextBox>())
            {
                txt.Clear();
            }
        }
        private void sacuvajbtn_Click(object sender, EventArgs e)
        {
            Sacuvaj();
        }

        public void DisableSacuvaj()
        {
            sacuvajbtn.Visible = false;
        }
        public void EnableSacuvaj()
        {
            sacuvajbtn.Visible = true;
        }
        public void DisableDodajOkruzenju()
        {
            btnDodajOkruzenju.Visible = false;
        }
        public void EnableDodajOkruzenju()
        {
            btnDodajOkruzenju.Visible = true;
        }

        public void DodajSobuHotelu(Soba s)
        {
            sobe.Add(s);
        }
        private void btnSoba_Click(object sender, EventArgs e)
        {
            NapraviHotel();
            hotelProvajder.AddHotel(h);
            FormaZaSobu.pripadaHotelu = h;
            FormaZaSobu.Visible = true;
            FormaZaSobu.BringToFront();
            FormaZaSobu.Hotel = this;
        }

        private void btnDodajOkruzenju_Click(object sender, EventArgs e)
        {
            Sacuvaj();
            okruzenje.DodajHotelOkruzenju(h);
            MessageBox.Show("Hotel je dodat okruzenju.");
            okruzenje.BringToFront();
            sacuvajbtn.Visible = true;
        }
    }
}
