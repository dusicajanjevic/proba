﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NHibernate;

namespace Baze3
{
    public partial class DodajKlijenta : UserControl
    {
        public Operacija operacija { get; set; }
        Provajderi.KlijentProvajder klijentProvajder;
        public int KlijentId { get; set; }
        public Entiteti.Klijent k;
        public DodajDete Dete { get; set; }
        List<Entiteti.Dete> deca;
        public User_Controls.DodajGrupu grupa { get; set; }
        public Entiteti.Grupa pripadaGrupi;

        public DodajKlijenta()
        {
            InitializeComponent();
            klijentProvajder = new Provajderi.KlijentProvajder();
            deca = new List<Entiteti.Dete>();
        }

        private void NapraviKlijenta()
        {
            k = new Entiteti.Klijent()
            {
                Ime = textBox2.Text,
                Prezime = textBox4.Text,
                Dat_rodj = DateTime.Parse(dateTimePicker1.Text),
                Jmbg = textBox3.Text
            };
        }
        private void Klijentbtn_Click(object sender, EventArgs e)
        {
            Provajderi.DeteProvajder deteProvajder = new Provajderi.DeteProvajder();

            NapraviKlijenta();
            switch (operacija)
            {
                case Operacija.Insert:
                    {
                        klijentProvajder.AddKlijent(k);

                        k.PripadaGrupi = pripadaGrupi;
                        foreach(Entiteti.Dete dete in deca)
                        {
                            dete.Roditelj = k;
                            if (deteProvajder.AddDete(dete) == 1)
                            {
                                k.Deca.Add(dete);
                                MessageBox.Show("Uspesan insert deteta.");
                            }
                            else
                                MessageBox.Show("Neuspesan insert deteta!");
                        }
                        if(klijentProvajder.UpdateKlijent(k.Id, k))
                        {
                            MessageBox.Show("Uspesan insert klijenta.");
                        }
                        else
                            MessageBox.Show("Neuspesan insert klijenta!");

                    }
                        break;
                case Operacija.Update:
                    {
                        if (klijentProvajder.UpdateKlijent(KlijentId, k))
                            MessageBox.Show("Uspesan update.");
                        else
                            MessageBox.Show("Neuspesan update!");
                    }
                    break;
            }
        }

        public void DodajDeteRoditelju(Entiteti.Dete dete)
        {
            deca.Add(dete);
        }
        private void button3_Click(object sender, EventArgs e) //kreiraj dete
        {
            Dete.Visible = true;
            Dete.BringToFront();
            Dete.operacija = Operacija.Insert;
            Dete.Roditelj = this;
        }

        private void button1_Click(object sender, EventArgs e)  //pridruzuje se grupi
        {
            NapraviKlijenta();
            this.operacija = Operacija.Insert;
            grupa.DisableSacuvaj();
            grupa.EnableDodajKlijenta();
            grupa.Visible = true;
            grupa.BringToFront();
            grupa.dodajKlijenta = this;
        }
    }
}
