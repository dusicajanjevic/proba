﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Baze3.Entiteti;
using Baze3.Provajderi;

namespace Baze3
{
    public partial class DodajApartman : UserControl
    {
        public Operacija operacija { get; set; }
        ApartmanProvajder apartmanProvajder;
        public int ApartmanId { get; set; }
        Apartman a;
        public DodajOkruzenje okruzenje { get; set; }

        public DodajApartman()
        {
            InitializeComponent();
            apartmanProvajder = new ApartmanProvajder();
        }
        
        public void DisableSacuvaj()
        {
            button1.Visible = false;
        }
        public void EnableSacuvaj()
        {
            button1.Visible = true;
        }
        public void DisablePovezi()
        {
            SOkrApbtn.Visible = false;
        }
        public void EnablePovezi()
        {
            SOkrApbtn.Visible = true;
        }

        private void NapraviApartman()
        {
            string tip;
            if (radioButton1.Checked)
            {
                tip = "Garsonjera";
                a = new Garsonjera()
                {
                    ImeVlasnika = imeAptxt.Text,
                    PrezimeVlasnika = prezimeAptxt.Text,
                    Telefon = telAptxt.Text,
                    TipApartmana = tip
                };
            }
            else
            {
                tip = "Sa kuhinjom";
                a = new SaKuhinjom()
                {
                    ImeVlasnika = imeAptxt.Text,
                    PrezimeVlasnika = prezimeAptxt.Text,
                    Telefon = telAptxt.Text,
                    TipApartmana = tip
                };
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            NapraviApartman();
            switch (operacija)
            {
                case Operacija.Insert:
                    {
                        if(apartmanProvajder.AddApartman(a)==1)
                            MessageBox.Show("Uspesan insert.");
                        else
                            MessageBox.Show("Neuspesan insert!");
                    } break;
                case Operacija.Update:
                    {
                        if (apartmanProvajder.UpdateApartman(ApartmanId, a))
                            MessageBox.Show("Uspesan update.");
                        else
                            MessageBox.Show("Neuspesan update!");
                    }
                    break;
            }
        }

        private void novoOkruzenjebtn_Click(object sender, EventArgs e)
        {

        }

        private void SOkrApbtn_Click(object sender, EventArgs e)
        {
            NapraviApartman();
            okruzenje.DodajApartmanOkruzenju(a);
            MessageBox.Show("Apartman je dodat okruzenju.");
            okruzenje.BringToFront();
            button1.Visible = true;
        }
    }
}
