﻿namespace Baze3
{
    partial class DodajApartman
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.lab = new System.Windows.Forms.Label();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.prezimeAptxt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.telAptxt = new System.Windows.Forms.TextBox();
            this.imeAptxt = new System.Windows.Forms.TextBox();
            this.SOkrApbtn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Controls.Add(this.lab);
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.prezimeAptxt);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.telAptxt);
            this.groupBox1.Controls.Add(this.imeAptxt);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(80, 22);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(214, 201);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Vlasnik apartmana";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(87, 173);
            this.radioButton2.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(94, 19);
            this.radioButton2.TabIndex = 5;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Sa kuhinjom";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // lab
            // 
            this.lab.AutoSize = true;
            this.lab.Location = new System.Drawing.Point(17, 84);
            this.lab.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lab.Name = "lab";
            this.lab.Size = new System.Drawing.Size(53, 15);
            this.lab.TabIndex = 7;
            this.lab.Text = "Prezime";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(87, 150);
            this.radioButton1.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(86, 19);
            this.radioButton1.TabIndex = 4;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Garsonjera";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 50);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 15);
            this.label7.TabIndex = 6;
            this.label7.Text = "Ime";
            // 
            // prezimeAptxt
            // 
            this.prezimeAptxt.Location = new System.Drawing.Point(97, 84);
            this.prezimeAptxt.Margin = new System.Windows.Forms.Padding(2);
            this.prezimeAptxt.Name = "prezimeAptxt";
            this.prezimeAptxt.Size = new System.Drawing.Size(76, 21);
            this.prezimeAptxt.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 118);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Telefon";
            // 
            // telAptxt
            // 
            this.telAptxt.Location = new System.Drawing.Point(97, 118);
            this.telAptxt.Margin = new System.Windows.Forms.Padding(2);
            this.telAptxt.Name = "telAptxt";
            this.telAptxt.Size = new System.Drawing.Size(76, 21);
            this.telAptxt.TabIndex = 3;
            // 
            // imeAptxt
            // 
            this.imeAptxt.Location = new System.Drawing.Point(97, 50);
            this.imeAptxt.Margin = new System.Windows.Forms.Padding(2);
            this.imeAptxt.Name = "imeAptxt";
            this.imeAptxt.Size = new System.Drawing.Size(76, 21);
            this.imeAptxt.TabIndex = 1;
            // 
            // SOkrApbtn
            // 
            this.SOkrApbtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(8)))), ((int)(((byte)(55)))));
            this.SOkrApbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SOkrApbtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SOkrApbtn.Location = new System.Drawing.Point(80, 292);
            this.SOkrApbtn.Margin = new System.Windows.Forms.Padding(2);
            this.SOkrApbtn.Name = "SOkrApbtn";
            this.SOkrApbtn.Size = new System.Drawing.Size(214, 47);
            this.SOkrApbtn.TabIndex = 6;
            this.SOkrApbtn.Text = "Povezi sa okruzenjem";
            this.SOkrApbtn.UseVisualStyleBackColor = false;
            this.SOkrApbtn.Click += new System.EventHandler(this.SOkrApbtn_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(8)))), ((int)(((byte)(55)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(80, 354);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(214, 32);
            this.button1.TabIndex = 7;
            this.button1.Text = "Sacuvaj ";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // DodajApartman
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LavenderBlush;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.SOkrApbtn);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "DodajApartman";
            this.Size = new System.Drawing.Size(368, 455);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lab;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox prezimeAptxt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox telAptxt;
        private System.Windows.Forms.TextBox imeAptxt;
        private System.Windows.Forms.Button SOkrApbtn;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Button button1;
    }
}
