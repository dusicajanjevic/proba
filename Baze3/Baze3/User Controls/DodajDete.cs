﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Baze3
{
    public partial class DodajDete : UserControl
    {
        public Operacija operacija { get; set; }
        public int DeteId { get; set; }
        public DodajKlijenta Roditelj { get; set; }
        Entiteti.Dete d;
        Provajderi.DeteProvajder deteProvajder;

        public DodajDete()
        {
            InitializeComponent();
            deteProvajder = new Provajderi.DeteProvajder();
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void NapraviDete()
        {
            d = new Entiteti.Dete()
            {
                Ime = imetxt.Text,
                Prezime = prezimetxt.Text,
                Uzrast = int.Parse(uzrasttxt.Text)
            };
        }
        private void button1_Click(object sender, EventArgs e)
        {
            NapraviDete();
            switch (operacija)
            {
                case Operacija.Insert:
                    {
                        Roditelj.DodajDeteRoditelju(d);
                        MessageBox.Show("Dete pridruzeno roditelju.");
                        Roditelj.BringToFront();
                    } break;
                case Operacija.Update:
                    {
                        if(deteProvajder.UpdateDete(DeteId, d))
                            MessageBox.Show("Uspesan update.");
                        else
                            MessageBox.Show("Neuspesan update!");
                    }
                    break;
            }
        }
    }
}
