﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NHibernate;

namespace Baze3
{
    public partial class DodajOkruzenje : UserControl
    {
        public Operacija operacija { get; set; }
        Provajderi.OkruzenjeProvajder okruzenjeProvajder;
        public int OkruzenjeId { get; set; }
        public int DodatneUslugeId { get; set; }
        Entiteti.Okruzenje ok;
        public DodajHotel FormaZaHotel { get; set; }
        public DodajApartman FormaZaApartman { get; set; }

        List<Entiteti.Hotel> hoteli;
        List<Entiteti.Apartman> apartmani;
        public User_Controls.DodajMedicinskiTretman tretman { get; set; }
        List<Entiteti.MedicinskiTretman> tretmani;
        List<Entiteti.DodatneUslugeOkruzenje> dodatneUsluge;

        public DodajOkruzenje()
        {
            InitializeComponent();
            okruzenjeProvajder = new Provajderi.OkruzenjeProvajder();
            hoteli = new List<Entiteti.Hotel>();
            apartmani = new List<Entiteti.Apartman>();
            tretmani = new List<Entiteti.MedicinskiTretman>();
            dodatneUsluge = new List<Entiteti.DodatneUslugeOkruzenje>();
        }

        private void NapraviOkruzenje()
        {
            if (comboBox1.Text == "Banja")
                ok = new Entiteti.Banja();
            else
                if (comboBox1.Text == "Spa centar")
                ok = new Entiteti.Spa();
            else
                ok = new Entiteti.BanjaSaSpa();

            ok.TipOkruzenja = comboBox1.Text;
        }
        private void button2_Click(object sender, EventArgs e)
        {
            NapraviOkruzenje();
            Provajderi.DodatneUslugeProvider dup = new Provajderi.DodatneUslugeProvider();
            Provajderi.HotelProvajder hup = new Provajderi.HotelProvajder();
            Provajderi.ApartmanProvajder app = new Provajderi.ApartmanProvajder();
            Provajderi.MedicinskiTretmanProvajder mtp = new Provajderi.MedicinskiTretmanProvajder();

            switch (operacija)
            {
                case Operacija.Insert:
                    {
                        if (okruzenjeProvajder.AddOkruzenje(ok) == 1)
                            MessageBox.Show("Uspesan insert.");
                        else
                            MessageBox.Show("Neuspesan insert!");

                        foreach (Entiteti.DodatneUslugeOkruzenje u in dodatneUsluge)
                        {
                            u.PripadaOkruznju = ok;
                            if (dup.AddDodatneUsluge(u) == 1)
                                MessageBox.Show("Uspesan insert dodatne usluge.");
                            else
                                MessageBox.Show("Neuspesan insert dodatne usluge!");
                        }
                        foreach (Entiteti.Hotel h in hoteli)
                        {
                            h.PripadaOkruzenju = ok;
                            if (hup.AddHotel(h) == 1)
                            {
                                MessageBox.Show("Uspesan insert hotela.");
                                ok.Hoteli.Add(h);
                            }
                            else
                                MessageBox.Show("Neuspesan insert hotela!");
                        }
                        foreach (Entiteti.Apartman ap in apartmani)
                        {
                            ap.PripadaOkruzenju = ok;
                            if (app.AddApartman(ap) == 1)
                            {
                                MessageBox.Show("Uspesan insert apartmana.");
                                ok.Apartmani.Add(ap);
                            }
                            else
                                MessageBox.Show("Neuspesan insert apartmana!");
                        }
                        foreach (Entiteti.MedicinskiTretman medtret in tretmani)
                        {
                            medtret.OkruzenjeTretman.Add(ok);
                            if (mtp.AddMedicinskiTretman(medtret) == 1)
                            {
                                MessageBox.Show("Uspesan insert medicinskog tretmana.");
                                ok.MedicinskiTretmani.Add(medtret);
                            }
                            else
                                MessageBox.Show("Neuspesan insert medicinskog tretmana!");

                            ok.MedicinskiTretmani.Add(medtret);
                        }

                        try
                        {
                            ISession s = DataLayer.GetSession();
                            s.Update(ok);
                            s.Flush();
                            s.Close();
                        }
                        catch (Exception ec)
                        {
                            //MessageBox.Show("greska" + ec);
                        }
                    } break;
                case Operacija.Update:
                    {
                        if (okruzenjeProvajder.UpdateOkruzenje(OkruzenjeId, ok))
                            MessageBox.Show("Uspesan insert.");
                        else
                            MessageBox.Show("Neuspesan insert!");
                    } break;
            }
        }

        private void button1_Click(object sender, EventArgs e) //dodatna usluga
        {
            Entiteti.DodatneUslugeOkruzenje dodatna = new Entiteti.DodatneUslugeOkruzenje()
            {
                Usluga = textBox1.Text
            };
            dodatneUsluge.Add(dodatna);
            textBox1.Clear();
        }

        public void DodajHotelOkruzenju(Entiteti.Hotel h)
        {
            hoteli.Add(h);
        }
        public void DodajApartmanOkruzenju(Entiteti.Apartman a)
        {
            apartmani.Add(a);
        }
        private void btnDodajHotel_Click(object sender, EventArgs e)
        {
            FormaZaHotel.DisableSacuvaj();
            FormaZaHotel.EnableDodajOkruzenju();
            FormaZaHotel.Visible = true;
            FormaZaHotel.BringToFront();
            FormaZaHotel.okruzenje = this;
        }

        public void DodajTretman(Entiteti.MedicinskiTretman m)
        {
            tretmani.Add(m);
        }
        private void btnDodajTretman_Click(object sender, EventArgs e)
        {
            tretman.DisableSacuvaj();
            tretman.EnablePovezi();
            tretman.Visible = true;
            tretman.BringToFront();
            tretman.okruzenje = this;
        }

        private void btnDodajApartman_Click(object sender, EventArgs e)
        {
            FormaZaApartman.DisableSacuvaj();
            FormaZaApartman.EnablePovezi();
            FormaZaApartman.Visible = true;
            FormaZaApartman.BringToFront();
            FormaZaApartman.okruzenje = this;
        }
    }
}
