﻿using Baze3.Entiteti;
using NHibernate;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Baze3.Provajderi
{
    class DodatneUslugeProvider
    {
        public IEnumerable<DodatneUslugeOkruzenje> GetDete()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<DodatneUslugeOkruzenje> dodatne = s.Query<DodatneUslugeOkruzenje>().Select(p => p);
            return dodatne;
        }

        public List<DodatneUslugeOkruzenje> Lista()
        {
            ISession s = DataLayer.GetSession();

            List<DodatneUslugeOkruzenje> dodatne = s.Query<DodatneUslugeOkruzenje>().Select(p => (p)).ToList();

            return dodatne;
        }

        public DodatneUslugeOkruzenje GetDodatneUsluge(int id)
        {
            ISession s = DataLayer.GetSession();

            return s.Query<DodatneUslugeOkruzenje>()
                    .Where(v => v.Id == id).Select(d => (d)).FirstOrDefault();
            //  .Select(p => p).FirstOrDefault();
        }

        public int AddDodatneUsluge(DodatneUslugeOkruzenje d)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(d);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public int RemoveDodatneUsluge(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                DodatneUslugeOkruzenje d = s.Load<DodatneUslugeOkruzenje>(id);

                s.Delete(d);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }
        }

        public bool UpdateDodatneUsluge(int id, DodatneUslugeOkruzenje d)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Query<DodatneUslugeOkruzenje>()
                      .Where(v => v.Id == id)
                      .UpdateBuilder()
                      .Set(c => c.Usluga, c => d.Usluga)
                      .Set(c => c.PripadaOkruznju, c => d.PripadaOkruznju)

                      .Update();
                return true;
            }
            catch (Exception) { return false; }
        }
    }
}