﻿using Baze3.Entiteti;
using NHibernate;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Baze3.Provajderi
{
    class GrupaProvajder
    {

        public IEnumerable<Grupa> GetGrupe()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<Grupa> g = s.Query<Grupa>()
                                                .Select(p => p);
            return g;
        }

        public List<Grupa> Lista()
        {
            ISession s = DataLayer.GetSession();

            List<Grupa> g = s.Query<Grupa>()
                                      .Select(p => (p)).ToList();

            return g;
        }

        public Grupa GetGrupa(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Grupa g = s.Query<Grupa>()
                    .Where(v => v.Id == id).Select(d => d).FirstOrDefault();

                s.Flush();
                s.Close();
                return g;
            }
            catch(Exception)
            {
                return null;
            }
        }

        public int AddGrupa(Grupa d)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(d);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public int vratiId(String naziv)
        {
            ISession s = DataLayer.GetSession();
            return s.Query<Grupa>()
                .Where(v => v.Naziv.Equals(naziv)).Select(d => (d.Id)).FirstOrDefault();
        }
        public int RemoveGrupa(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Grupa d = s.Load<Grupa>(id);

                s.Delete(d);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }

        }

        public void UpdateGrupa(int id, Grupa d)
        {
            ISession s = DataLayer.GetSession();

            s.Query<Grupa>()
                  .Where(v => v.Id == id)
                  .UpdateBuilder()
                  .Set(c => c.Naziv, c => d.Naziv)

                  .Update();
        }

        public Grupa VratiGrupuPoNazivu(String naziv)
        {
            ISession s = DataLayer.GetSession();
            Grupa g = s.Query<Grupa>()
                .Where(v => v.Naziv.Equals(naziv)).Select(d => d).FirstOrDefault();
            s.Close();

            return g;
        }

        public bool DodajKlijenta(Klijent k, int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Grupa g = s.Query<Grupa>()
                    .Where(v => v.Id == id).Select(d => d).FirstOrDefault();

                k.PripadaGrupi = g;
                s.Save(k);

                g.Klijenti.Add(k);
                s.SaveOrUpdate(g);

                s.Flush();
                s.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}