﻿using Baze3.Entiteti;
using NHibernate;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Baze3.Provajderi
{
    class OkruzenjeProvajder
    {

        public IEnumerable<Okruzenje> GetOkruzenje()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<Okruzenje> Okruzenje = s.Query<Okruzenje>()
                                                .Select(p => p);
            return Okruzenje;
        }

        public List<Okruzenje> Lista()
        {
            ISession s = DataLayer.GetSession();

            List<Okruzenje> Okruzenje = s.Query<Okruzenje>()
                                                .Select(p => (p)).ToList();

            return Okruzenje;
        }

        public Okruzenje GetOkruzenje(int id)
        {
            ISession s = DataLayer.GetSession();

            return s.Query<Okruzenje>()
                .Where(v => v.Id == id).Select(d => (d)).FirstOrDefault();
            //  .Select(p => p).FirstOrDefault();
        }

        public int AddOkruzenje(Okruzenje d)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(d);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int RemoveOkruzenje(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Okruzenje d = s.Load<Okruzenje>(id);

                s.Delete(d);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }
        }

        public bool UpdateOkruzenje(int id, Okruzenje d)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Query<Okruzenje>()
                      .Where(v => v.Id == id)
                      .UpdateBuilder()
                      .Set(c => c.TipOkruzenja, c => d.TipOkruzenja)

                      .Update();
                return true;
            }
            catch (Exception) { return false; }
        }
    }
}
