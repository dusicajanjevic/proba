﻿using Baze3.Entiteti;
using NHibernate;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Baze3.Provajderi
{
    class RezervacijaProvajder
    {

        public IEnumerable<Rezervacija> GetRezervacija()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<Rezervacija> Rez = s.Query<Rezervacija>()
                                                .Select(p => p);
            return Rez;
        }

        public List<Rezervacija> Lista()
        {
            ISession s = DataLayer.GetSession();

            List<Rezervacija> Rezer = s.Query<Rezervacija>()
                                                .Select(p => (p)).ToList();

            return Rezer;
        }

        public Rezervacija GetRezervacija(int id)
        {
            ISession s = DataLayer.GetSession();

            return s.Query<Rezervacija>()
                .Where(v => v.Id == id).Select(d => (d)).FirstOrDefault();
            //  .Select(p => p).FirstOrDefault();
        }

        public int AddRezervacija(Rezervacija d)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(d);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int RemoveRezervacija(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Rezervacija d = s.Load<Rezervacija>(id);

                s.Delete(d);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }
        }

        public bool UpdateRezervacija(int id, Rezervacija d)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Query<Rezervacija>()
                      .Where(v => v.Id == id)
                      .UpdateBuilder()
                      .Set(c => c.TipRezervacije, c => d.TipRezervacije)
                      .Set(c => c.TipUsluge, c => d.TipUsluge)
                      .Set(c => c.Od, c => d.Od)
                      .Set(c => c.Do, c => d.Do)
                      .Set(c => c.RezApartman, c => d.RezApartman)

                      .Update();
                return true;
            }
            catch (Exception) { return false; }
        }

        public bool PraviRez(Klijent k, Rezervacija r)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                PraviRez pr = new PraviRez();
                pr.ImeKlijenta = k.Ime;
                pr.PrezimeKlijenta = k.Prezime;
                pr.JmbgKlijenta = k.Jmbg;
                pr.DatRodj = k.Dat_rodj;
                pr.Od = r.Od;
                pr.Do = r.Do;
                pr.TipUsluge = r.TipUsluge;
                pr.Klijent = k;
                pr.Rezervacija = r;

                s.Save(pr);
                s.Flush();
                s.Close();

                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }
    }
}
