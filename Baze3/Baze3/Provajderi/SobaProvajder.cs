﻿using Baze3.Entiteti;
using NHibernate;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Baze3.Provajderi
{
    class SobaProvajder
    {
        public IEnumerable<Soba> GetSobe()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<Soba> Soba = s.Query<Soba>()
                                                .Select(p => p);
            return Soba;
        }

        public List<Soba> Lista()
        {
            ISession s = DataLayer.GetSession();

            List<Soba> Soba = s.Query<Soba>()
                                                .Select(p => (p)).ToList();

            return Soba;
        }

        public Soba GetSoba(int id)
        {
            ISession s = DataLayer.GetSession();

            return s.Query<Soba>()
                .Where(v => v.Id == id).Select(d => d).FirstOrDefault();
        }

        public int AddSoba(Soba d)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(d);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int RemoveSoba(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Soba d = s.Load<Soba>(id);

                s.Delete(d);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }
        }

        public bool UpdateSoba(int id, Soba d)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Query<Soba>()
                      .Where(v => v.Id == id)
                      .UpdateBuilder()
                      .Set(c => c.Broj, c => d.Broj)
                      .Set(c => c.Sprat, c => d.Sprat)
                      .Set(c => c.Radio, c => d.Radio)
                      .Set(c => c.Tv, c => d.Tv)
                      .Set(c => c.Kupatilo, c => d.Kupatilo)
                      .Set(c => c.Klima, c => d.Klima)
                      .Set(c => c.Slobodno, c => d.Slobodno)
                      .Set(c => c.More, c => d.More)
                      .Set(c => c.Frizider, c => d.Frizider)
                      .Set(c => c.Tip, c => d.Tip)
                      .Set(c => c.PripadaHotelu, c => d.PripadaHotelu)

                      .Update();
                return true;
            }
            catch (Exception) { return false; }
        }

        public List<Usluga> UslugeIzVezanaJe(int idSobe)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                List<Usluga> usluge = s.Query<VezanaJe>()
                    .Where(v => v.Soba.Id == idSobe).Select(d => d.Usluga).ToList();

                s.Flush();
                s.Close();
                return usluge;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
