﻿using Baze3.Entiteti;
using NHibernate;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Baze3.Provajderi
{
    class KlijentProvajder
    {
        public IEnumerable<Klijent> GetKlijenti()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<Klijent> Klijent = s.Query<Klijent>()
                                                .Select(p => p);
            return Klijent;
        }

        public List<Klijent> Lista()
        {
            ISession s = DataLayer.GetSession();

            List<Klijent> Klijent = s.Query<Klijent>()
                                                .Select(p => (p)).ToList();

            return Klijent;
        }

        public Klijent GetKlijent(int id)
        {
            ISession s = DataLayer.GetSession();

            return s.Query<Klijent>()
                .Where(v => v.Id == id).Select(d => (d)).FirstOrDefault();
            //  .Select(p => p).FirstOrDefault();
        }

        public int AddKlijent(Klijent k)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(k);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int RemoveKlijent(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Klijent d = s.Load<Klijent>(id);

                s.Delete(d);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }
        }

        public bool UpdateKlijent(int id, Klijent d)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Query<Klijent>()
                      .Where(v => v.Id == id)
                      .UpdateBuilder()
                      .Set(c => c.Ime, c => d.Ime)
                      .Set(c => c.Prezime, c => d.Prezime)
                      .Set(c => c.Dat_rodj, c => d.Dat_rodj)
                      .Set(c => c.Jmbg, c => d.Jmbg)
                      .Set(c => c.PripadaGrupi, c => d.PripadaGrupi)

                      .Update();
                return true;
            }
            catch (Exception) { return false; }
        }
    }
}