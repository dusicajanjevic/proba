﻿using Baze3.Entiteti;
using NHibernate;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Baze3.Provajderi
{
    class HotelProvajder
    {

        public IEnumerable<Hotel> GetHotele()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<Hotel> Hoteli = s.Query<Hotel>()
                                                .Select(p => p);
            return Hoteli;
        }

        public List<Hotel> Lista()
        {
            ISession s = DataLayer.GetSession();

            List<Hotel> Hoteli = s.Query<Hotel>()
                                                .Select(p => (p)).ToList();

            return Hoteli;
        }

        public Hotel GetHotel(int id)
        {
            ISession s = DataLayer.GetSession();

            return s.Query<Hotel>()
                .Where(v => v.IdSmestaja == id).Select(d => (d)).FirstOrDefault();
            //  .Select(p => p).FirstOrDefault();
        }
        public bool UpdateHotel(int id , Hotel h)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Query<Hotel>()
                      .Where(v => v.IdSmestaja == id)
                      .UpdateBuilder()
                      .Set(c => c.Adresa, c => h.Adresa)
                      .Set(c => c.Br_lezaja, c => h.Br_lezaja)
                      .Set(c => c.Direktor, c => h.Direktor)
                      .Set(c => c.Grad, c => h.Grad)
                      .Set(c => c.Ime, c => h.Ime)
                      .Set(c => c.Restoran_mesta, c => h.Restoran_mesta)
                      .Set(c => c.Sifra, c => h.Sifra)
                      .Set(c => c.Kategorija, c => h.Kategorija)

                      .Update();
                return true;
            }
            catch (Exception) { return false; }
        }
        public int AddHotel(Hotel d)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(d);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public int RemoveHotel(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Hotel d = s.Load<Hotel>(id);

                s.Delete(d);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }

        }
    }
}
