﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Baze3.Entiteti;
using NHibernate.Linq;

namespace Baze3.Provajderi
{
    class ApartmanProvajder
    {
        public IEnumerable<Apartman> GetApartmane()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<Apartman> Apartman = s.Query<Apartman>()
                                                .Select(p => p);
            return Apartman;
        }

        public List<Apartman> Lista()
        {
            ISession s = DataLayer.GetSession();

            List<Apartman> Apartman = s.Query<Apartman>()
                                                .Select(p => (p)).ToList();

            return Apartman;
        }

        public Apartman GetApartman(int id)
        {
            ISession s = DataLayer.GetSession();

            return s.Query<Apartman>()
                .Where(v => v.IdSmestaja == id).Select(d => (d)).FirstOrDefault();
            //  .Select(p => p).FirstOrDefault();
        }

        public int AddApartman(Apartman d)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(d);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public int RemoveApartman(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Apartman d = s.Load<Apartman>(id);

                s.Delete(d);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }

        }

        public bool UpdateApartman(int id, Apartman a)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Query<Apartman>()
                      .Where(v => v.IdSmestaja == id)
                      .UpdateBuilder()
                      .Set(c => c.ImeVlasnika, c => a.ImeVlasnika)
                      .Set(c => c.PrezimeVlasnika, c => a.PrezimeVlasnika)
                      .Set(c => c.Telefon, c => a.Telefon)
                      .Set(c => c.TipApartmana, c => a.TipApartmana)

                      .Update();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
