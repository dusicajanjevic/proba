﻿using Baze3.Entiteti;
using NHibernate;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Baze3.Provajderi
{
    class MedicinskiTretmanProvajder
    {

        public IEnumerable<MedicinskiTretman> GetMedicinskiTretmane()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<MedicinskiTretman> MedicinskiTretman = s.Query<MedicinskiTretman>()
                                                .Select(p => p);
            return MedicinskiTretman;
        }

        public List<MedicinskiTretman> Lista()
        {
            ISession s = DataLayer.GetSession();

            List<MedicinskiTretman> MedicinskiTretman = s.Query<MedicinskiTretman>()
                                                .Select(p => (p)).ToList();

            return MedicinskiTretman;
        }

        public MedicinskiTretman GetMedicinskiTretman(int id)
        {
            ISession s = DataLayer.GetSession();

            return s.Query<MedicinskiTretman>()
                .Where(v => v.Id == id).Select(d => (d)).FirstOrDefault();
            //  .Select(p => p).FirstOrDefault();
        }

        public int AddMedicinskiTretman(MedicinskiTretman d)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(d);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int RemoveMedicinskiTretman(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                MedicinskiTretman d = s.Load<MedicinskiTretman>(id);

                s.Delete(d);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }
        }

        public bool UpdateMedicinskiTretman(int id, MedicinskiTretman d)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Query<MedicinskiTretman>()
                      .Where(v => v.Id == id)
                      .UpdateBuilder()
                      .Set(c => c.ImeLekara, c => d.ImeLekara)
                      .Set(c => c.Specijalnost, c => d.Specijalnost)

                      .Update();
                return true;
            }
            catch (Exception) { return false; }
        }
    }
}
