﻿using Baze3.Entiteti;
using NHibernate;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Baze3.Provajderi
{
    class UslugaProvider
    {
        public IEnumerable<Usluga> GetUsluge()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<Usluga> usluge = s.Query<Usluga>()
                                                .Select(p => p);
            return usluge;
        }

        public List<Usluga> Lista()
        {
            ISession s = DataLayer.GetSession();

            List<Usluga> usluge = s.Query<Usluga>()
                                                .Select(p => (p)).ToList();

            return usluge;
        }

        public Usluga GetUsluga(int id)
        {
            ISession s = DataLayer.GetSession();

            return s.Query<Usluga>()
                .Where(v => v.Id == id).Select(d => (d)).FirstOrDefault();
            //  .Select(p => p).FirstOrDefault();
        }

        public int AddUsluga(Usluga d)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(d);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int RemoveUsluga(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Usluga d = s.Load<Usluga>(id);

                s.Delete(d);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public void UpdateUsluga(int id, Usluga usl)
        {
            ISession s = DataLayer.GetSession();

            Usluga usluga = s.Query<Usluga>()
                  .Where(v => v.Id == id).Select(uslugaa => uslugaa).FirstOrDefault();
                  //.UpdateBuilder()
                  //.Set(c => c.ImaHotele, c => d.ImaHotele)
                  //.Update();
            foreach(Hotel hotel in usl.ImaHotele)
            {
                if (!usluga.ImaHotele.Contains(hotel))
                    usluga.ImaHotele.Add(hotel);
            }

            s.SaveOrUpdate(usluga);
            s.Flush();
            s.Close();
        }
    }
}