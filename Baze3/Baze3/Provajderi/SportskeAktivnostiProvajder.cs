﻿using Baze3.Entiteti;
using NHibernate;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Baze3.Provajderi
{
    class SportskeAktivnostiProvajder
    {
        public IEnumerable<SportskeAktivnosti> GetSportskeAktivnosti()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<SportskeAktivnosti> sportskeAktivnosti = s.Query<SportskeAktivnosti>()
                                                .Select(p => p);
            return sportskeAktivnosti;
        }

        public List<SportskeAktivnosti> Lista()
        {
            ISession s = DataLayer.GetSession();

            List<SportskeAktivnosti> SportskeAktivnosti = s.Query<SportskeAktivnosti>()
                                                .Select(p => (p)).ToList();

            return SportskeAktivnosti;
        }

        public SportskeAktivnosti GetSportskeAktivnosti(int id)
        {
            ISession s = DataLayer.GetSession();

            return s.Query<SportskeAktivnosti>()
                .Where(v => v.Id == id).Select(d => (d)).FirstOrDefault();
            //  .Select(p => p).FirstOrDefault();
        }

        public int AddSportskeAktivnosti(SportskeAktivnosti d)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(d);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int RemoveSportskeAktivnosti(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                SportskeAktivnosti d = s.Load<SportskeAktivnosti>(id);

                s.Delete(d);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }
        }

        public void UpdateSportskeAktivnost(int id, SportskeAktivnosti d)
        {
            ISession s = DataLayer.GetSession();

            s.Query<SportskeAktivnosti>()
                  .Where(v => v.Id == id)
                  .UpdateBuilder()
                  .Set(c => c.Aktivnost, c => d.Aktivnost)
                  .Set(c => c.PripadaHotelu, c => d.PripadaHotelu)

                  .Update();
        }
    }
}

