﻿using Baze3.Entiteti;
using NHibernate;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Baze3.Provajderi
{
    class DeteProvajder
    {
        public IEnumerable<Dete> GetDete()
        {
            ISession s = DataLayer.GetSession();
        
            IEnumerable<Dete> dete = s.Query<Dete>().Select(p =>p);
            return dete;
        }

        public List<Dete> Lista()
        {
            ISession s = DataLayer.GetSession();

            List<Dete> dete = s.Query<Dete>().Select(p =>(p)).ToList();

            return dete;
        }

        public Dete GetDete(int id)
        {
            ISession s = DataLayer.GetSession();

            return s.Query<Dete>()
                    .Where(v => v.Id == id).Select(d => (d)).FirstOrDefault();
                //  .Select(p => p).FirstOrDefault();
        }

        public int AddDete(Dete d)
        {
                try
                {
                    ISession s = DataLayer.GetSession();

                    s.Save(d);

                    s.Flush();
                    s.Close();

                    return 1;
                }
                catch (Exception ec)
                {
                    return -1;
                }
        }

            public int RemoveDete(int id)
            {
                try
                {
                    ISession s = DataLayer.GetSession();

                    Dete d = s.Load<Dete>(id);

                    s.Delete(d);

                    s.Flush();
                    s.Close();

                    return 1;
                }
                catch (Exception exc)
                {
                    return -1;
                }
            }

        public bool UpdateDete(int id, Dete d)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Query<Dete>()
                      .Where(v => v.Id == id)
                      .UpdateBuilder()
                      .Set(c => c.Ime, c => d.Ime)
                      .Set(c => c.Prezime, c => d.Prezime)
                      .Set(c => c.Uzrast, c => d.Uzrast)
                      .Set(c => c.Roditelj, c => d.Roditelj)

                      .Update();

                return true;
            }
            catch(Exception)
            { return false; }
        }
    }
}
