﻿using System;
using System.Collections.Generic;
using System.Linq;
using BPdeo1.DTOs;
using BPdeo1.Entiteti;
using NHibernate;
using System.Text;
using System.Threading.Tasks;

namespace BPdeo1.Data_Providers
{
   public class BanjaProvider
    {

        public IEnumerable<BanjaView> GetBanja()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<BanjaView> banje = s.Query<Banja>()
                                                .Select(p => new BanjaView(p));
            return banje;
        }

        public BanjaView GetBanjaView(int id)
        {
            ISession s = DataLayer.GetSession();

            Banja banja= s.Query<Banja>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();

            if (banja == null) return new BanjaView();

            return new BanjaView(banja);
        }

        public int AddBanja(Banja b)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(b);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int EditBanja(Banja b)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Banja banja = s.Query<Banja>()
                .Where(v => v.Id == b.Id).Select(p => p).FirstOrDefault();

                banja.TipOkruzenja = b.TipOkruzenja;

                s.Update(banja);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int RemoveBanja(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Banja b = s.Load<Banja>(id);

                s.Delete(b);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }
        }
    }
}
