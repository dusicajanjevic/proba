﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BPdeo1.DTOs;
using BPdeo1.Entiteti;
using NHibernate;

namespace BPdeo1.Data_Providers
{
    public class RezervacijaHotelaProvider
    {
        public IEnumerable<RezervacijaHotelaView> GetRezervacijeHotela()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<RezervacijaHotelaView> rez = s.Query<RezervacijaHotela>()
                                                .Select(p => new RezervacijaHotelaView(p));
            return rez;
        }

        public RezervacijaHotelaView GetRezervacijaHotelaView(int id)
        {
            ISession s = DataLayer.GetSession();

            RezervacijaHotela RezervacijaHotela = s.Query<RezervacijaHotela>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();

            if (RezervacijaHotela == null) return new RezervacijaHotelaView();

            return new RezervacijaHotelaView(RezervacijaHotela);
        }

        public int AddRezervacijaHotela(RezervacijaHotela a)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(a);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int EditRezervacijaHotela(RezervacijaHotela r)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                RezervacijaHotela rez = s.Query<RezervacijaHotela>()
                .Where(v => v.Id == r.Id).Select(p => p).FirstOrDefault();

                rez.TipRezervacije = r.TipRezervacije;
                rez.TipUsluge = r.TipUsluge;
                rez.Od = r.Od;
                rez.Do = r.Do;

                s.Update(rez);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int RemoveRezervacijaHotela(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                RezervacijaHotela a = s.Load<RezervacijaHotela>(id);

                s.Delete(a);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }
        }
    }
}
