﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BPdeo1.DTOs;
using BPdeo1.Entiteti;
using NHibernate;

namespace BPdeo1.Data_Providers
{
    public class TrokrevetnaProvider
    {

        public IEnumerable<TrokrevetnaView> GetTrokrevetne()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<TrokrevetnaView> sobe = s.Query<Trokrevetna>()
                                                .Select(p => new TrokrevetnaView(p));
            return sobe;
        }

        public TrokrevetnaView GetTrokrevetnaView(int id)
        {
            ISession s = DataLayer.GetSession();

            Trokrevetna soba = s.Query<Trokrevetna>()
                    .Where(v => v.Id == id).Select(p => p).FirstOrDefault();

            if (soba == null) return new TrokrevetnaView();

            return new TrokrevetnaView(soba);
        }

        public int AddTrokrevetna(Trokrevetna ts)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(ts);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int EditTrokrevetna(Trokrevetna so)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Trokrevetna soba = s.Query<Trokrevetna>()
                    .Where(v => v.Id == so.Id).Select(p => p).FirstOrDefault();

                soba.Broj = so.Broj;
                soba.Sprat = so.Sprat;
                soba.Radio = so.Radio;
                soba.Tv = so.Tv;
                soba.Kupatilo = so.Kupatilo;
                soba.Klima = so.Klima;
                soba.Slobodno = so.Slobodno;
                soba.More = so.More;
                soba.Frizider = so.Frizider;
                soba.Tip = so.Tip;

                s.Update(soba);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int RemoveTrokrevetna(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Trokrevetna a = s.Load<Trokrevetna>(id);

                s.Delete(a);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }
        }
    }
}
