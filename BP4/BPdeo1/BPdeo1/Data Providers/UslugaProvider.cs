﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BPdeo1.DTOs;
using BPdeo1.Entiteti;
using NHibernate;

namespace BPdeo1.Data_Providers
{
    public class UslugaProvider
    {
        public IEnumerable<UslugaView> GetUsluge()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<UslugaView> usluge = s.Query<Usluga>()
                                                .Select(p => new UslugaView(p));
            return usluge;
        }

        public UslugaView GetUslugaView(int id)
        {
            ISession s = DataLayer.GetSession();

            Usluga usluga = s.Query<Usluga>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();

            if (usluga == null) return new UslugaView();

            return new UslugaView(usluga);
        }

             

    }
}
