﻿using BPdeo1.DTOs;
using BPdeo1.Entiteti;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPdeo1.Data_Providers
{
    public class GarsonjeraProvider
    {
        public IEnumerable<GarsonjeraView> GetGarsonjere()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<GarsonjeraView> garsonjere = s.Query<Garsonjera>()
                                                .Select(p => new GarsonjeraView(p));
            return garsonjere;
        }

        public GarsonjeraView GetGarsonjeraView(int id)
        {
            ISession s = DataLayer.GetSession();

            Garsonjera g = s.Query<Garsonjera>()
                .Where(v => v.IdSmestaja == id).Select(p => p).FirstOrDefault();

            if (g == null) return new GarsonjeraView();

            return new GarsonjeraView(g);
        }

        public int AddGarsonjera(Garsonjera g)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(g);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int EditGarsonjera(Garsonjera g)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Garsonjera garsonjera = s.Query<Garsonjera>()
                    .Where(v => v.IdSmestaja == g.IdSmestaja).Select(p => p).FirstOrDefault();

                garsonjera.ImeVlasnika = g.ImeVlasnika;
                garsonjera.PrezimeVlasnika = g.PrezimeVlasnika;
                garsonjera.Telefon = g.Telefon;

                s.Update(garsonjera);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int RemoveGarsonjera(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Garsonjera g = s.Load<Garsonjera>(id);

                s.Delete(g);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }
        }
    }
}

