﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BPdeo1.DTOs;
using BPdeo1.Entiteti;
using NHibernate;

namespace BPdeo1.Data_Providers
{
    public class SaKuhinjomProvider
    {
        public IEnumerable<SaKuhinjomView> GetSaKuhinjom()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<SaKuhinjomView> saKuhinjom = s.Query<SaKuhinjom>()
                                                .Select(p => new SaKuhinjomView(p));
            return saKuhinjom;
        }

        public SaKuhinjomView GetSaKuhinjomView(int id)
        {
            ISession s = DataLayer.GetSession();

            SaKuhinjom sk = s.Query<SaKuhinjom>()
                .Where(v => v.IdSmestaja == id).Select(p => p).FirstOrDefault();

            if (sk == null) return new SaKuhinjomView();

            return new SaKuhinjomView(sk);
        }

        public int AddSaKuhinjom(SaKuhinjom sk)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(sk);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int EditSaKuhinjom(SaKuhinjom sk)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                SaKuhinjom saKuhinjom = s.Query<SaKuhinjom>()
                    .Where(v => v.IdSmestaja == sk.IdSmestaja).Select(p => p).FirstOrDefault();

                saKuhinjom.ImeVlasnika = sk.ImeVlasnika;
                saKuhinjom.PrezimeVlasnika = sk.PrezimeVlasnika;
                saKuhinjom.Telefon = sk.Telefon;

                s.Update(saKuhinjom);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int RemoveSaKuhinjom(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                SaKuhinjom sk = s.Load<SaKuhinjom>(id);

                s.Delete(sk);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }
        }
    }
}

