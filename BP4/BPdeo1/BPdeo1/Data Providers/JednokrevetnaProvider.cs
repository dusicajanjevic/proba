﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BPdeo1.DTOs;
using BPdeo1.Entiteti;
using NHibernate;

namespace BPdeo1.Data_Providers
{
   public class JednokrevetnaProvider
    {

        public IEnumerable<JednokrevetnaView> GetJednokrevetne()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<JednokrevetnaView> sobe = s.Query<Jednokrevetna>()
                                                .Select(p => new JednokrevetnaView(p));
            return sobe;
        }

        public JednokrevetnaView GetJednokrevetnaView(int id)
        {
            ISession s = DataLayer.GetSession();

            Jednokrevetna soba = s.Query<Jednokrevetna>()
                    .Where(v => v.Id == id).Select(p => p).FirstOrDefault();

            if (soba == null) return new JednokrevetnaView();

            return new JednokrevetnaView(soba);
        }

        public int AddJednokrevetna(Jednokrevetna js)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(js);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int EditJednokrevetna(Jednokrevetna so)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Jednokrevetna soba = s.Query<Jednokrevetna>()
                    .Where(v => v.Id == so.Id).Select(p => p).FirstOrDefault();

                soba.Broj = so.Broj;
                soba.Sprat = so.Sprat;
                soba.Radio = so.Radio;
                soba.Tv = so.Tv;
                soba.Kupatilo = so.Kupatilo;
                soba.Klima = so.Klima;
                soba.Slobodno = so.Slobodno;
                soba.More = so.More;
                soba.Frizider = so.Frizider;
                soba.Tip = so.Tip;

                s.Update(soba);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int RemoveJednokrevetna(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Jednokrevetna a = s.Load<Jednokrevetna>(id);

                s.Delete(a);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }
        }
    }
}
