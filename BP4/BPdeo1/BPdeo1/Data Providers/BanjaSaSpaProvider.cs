﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BPdeo1.DTOs;
using BPdeo1.Entiteti;
using NHibernate;

namespace BPdeo1.Data_Providers
{
    public class BanjaSaSpaProvider
    {

        public IEnumerable<BanjaSaSpaView> GetBanjaSaSpa()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<BanjaSaSpaView> banjeSaSpa = s.Query<BanjaSaSpa>()
                                                .Select(p => new BanjaSaSpaView(p));
            return banjeSaSpa;
        }

        public BanjaSaSpaView GetBanjaSaSpaView(int id)
        {
            ISession s = DataLayer.GetSession();

            BanjaSaSpa banjaSaSpa = s.Query<BanjaSaSpa>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();

            if (banjaSaSpa == null) return new BanjaSaSpaView();

            return new BanjaSaSpaView(banjaSaSpa);
        }

        public int AddBanjaSaSpa(BanjaSaSpa b)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(b);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int EditBanjaSaSpa(BanjaSaSpa b)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                BanjaSaSpa banjaSaSpa = s.Query<BanjaSaSpa>()
                .Where(v => v.Id == b.Id).Select(p => p).FirstOrDefault();

                banjaSaSpa.TipOkruzenja = b.TipOkruzenja;

                s.Update(banjaSaSpa);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int RemoveBanjaSaSpa(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                BanjaSaSpa b = s.Load<BanjaSaSpa>(id);

                s.Delete(b);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }
        }
    }
}
