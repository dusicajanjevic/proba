﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BPdeo1.DTOs;
using BPdeo1.Entiteti;
using NHibernate;

namespace BPdeo1.Data_Providers
{
    public class DvokrevetnaProvider
    {

        public IEnumerable<DvokrevetnaView> GetDvokrevetne()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<DvokrevetnaView> sobe = s.Query<Dvokrevetna>()
                                                .Select(p => new DvokrevetnaView(p));
            return sobe;
        }

        public DvokrevetnaView GetDvokrevetnaView(int id)
        {
            ISession s = DataLayer.GetSession();

            Dvokrevetna soba = s.Query<Dvokrevetna>()
                    .Where(v => v.Id == id).Select(p => p).FirstOrDefault();

            if (soba == null) return new DvokrevetnaView();

            return new DvokrevetnaView(soba);
        }

        public int AddDvokrevetna(Dvokrevetna ds)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(ds);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int EditDvokrevetna(Dvokrevetna so)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Dvokrevetna soba = s.Query<Dvokrevetna>()
                    .Where(v => v.Id == so.Id).Select(p => p).FirstOrDefault();

                soba.Broj = so.Broj;
                soba.Sprat = so.Sprat;
                soba.Radio = so.Radio;
                soba.Tv = so.Tv;
                soba.Kupatilo = so.Kupatilo;
                soba.Klima = so.Klima;
                soba.Slobodno = so.Slobodno;
                soba.More = so.More;
                soba.Frizider = so.Frizider;
                soba.Tip = so.Tip;

                s.Update(soba);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int RemoveDvokrevetna(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Dvokrevetna a = s.Load<Dvokrevetna>(id);

                s.Delete(a);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }
        }
    }

}