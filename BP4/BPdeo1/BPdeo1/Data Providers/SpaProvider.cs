﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BPdeo1.DTOs;
using BPdeo1.Entiteti;
using NHibernate;

namespace BPdeo1.Data_Providers
{
    public class SpaProvider
    {

        public IEnumerable<SpaView> GetSpa()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<SpaView> spa = s.Query<Spa>()
                                                .Select(p => new SpaView(p));
            return spa;
        }

        public SpaView GetSpaView(int id)
        {
            ISession s = DataLayer.GetSession();

            Spa spa = s.Query<Spa>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();

            if (spa == null) return new SpaView();

            return new SpaView(spa);
        }

        public int AddSpa(Spa spa)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(spa);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int EditSpa(Spa b)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Spa spa = s.Query<Spa>()
                .Where(v => v.Id == b.Id).Select(p => p).FirstOrDefault();

                spa.TipOkruzenja = b.TipOkruzenja;

                s.Update(spa);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int RemoveSpa(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                 Spa b = s.Load<Spa>(id);

                s.Delete(b);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }
        }
    }
}

