﻿using BPdeo1.Entiteti;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPdeo1.DTOs
{
    public class TrokrevetnaView
    {

        public int SobaId { get; set; }
        public int Broj { get; set; }
        public int Sprat { get; set; }
        public string Radio { get; set; }
        public string Tv { get; set; }
        public string Kupatilo { get; set; }
        public string Klima { get; set; }
        public string Slobodno { get; set; }
        public string More { get; set; }
        public string Frizider { get; set; }
        public string Tip { get; set; }  


        public TrokrevetnaView(Trokrevetna s)
        {
            this.SobaId = s.Id;
            this.Broj = s.Broj;
            this.Sprat = s.Sprat;
            this.Radio = s.Radio;
            this.Tv = s.Tv;
            this.Kupatilo = s.Kupatilo;
            this.Klima = s.Klima;
            this.Slobodno = s.Slobodno;
            this.More = s.More;
            this.Frizider = s.Frizider;
            this.Tip = "Trokrevetna";
        }

        public TrokrevetnaView()
        { }
    }
}
