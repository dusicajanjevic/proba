﻿using BPdeo1.Entiteti;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPdeo1.DTOs
{
    public class RezervacijaHotelaView
    {
        public int RezervacijaHotelaId { get; set; }
        public string TipUsluge { get; set; }
        public string TipRezervacije { get; set; }


        public DateTime Od { get; set; }
        public DateTime Do { get; set; }

        public RezervacijaHotelaView(RezervacijaHotela r)
        {
            this.RezervacijaHotelaId = r.Id;
            this.TipUsluge = r.TipUsluge;
            this.TipRezervacije = "HOTEL";
            this.Od = r.Od;
            this.Do = r.Do;
        }
        public RezervacijaHotelaView()
        { }
    }
}
