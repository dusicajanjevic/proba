﻿using BPdeo1.Entiteti;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPdeo1.DTOs
{
   public class SpaView
    {

        public int SpaId { get; set; }
        public string TipOkruzenja { get; set; }


        public SpaView(Spa s)
        {
            this.SpaId = s.Id;
            this.TipOkruzenja = "Spa";

        }
        public SpaView()
        { }
    }
}
