﻿using BPdeo1.Entiteti;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPdeo1.DTOs
{
    public class BanjaSaSpaView
    {

        public int BanjaSaSpaId { get; set; }
        public string TipOkruzenja { get; set; }


        public BanjaSaSpaView(BanjaSaSpa b)
        {
            this.BanjaSaSpaId = b.Id;
            this.TipOkruzenja = "BanjaSaSpa";

        }
        public BanjaSaSpaView()
        { }
    }
}
