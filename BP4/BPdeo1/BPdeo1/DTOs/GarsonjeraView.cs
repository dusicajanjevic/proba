﻿using BPdeo1.Entiteti;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPdeo1.DTOs
{
    public class GarsonjeraView
    {
      
            public int IdSmestaja { get; set; }
            public string ImeVlasnika { get; set; }
            public string PrezimeVlasnika { get; set; }
            public string Telefon { get; set; }
           
            public GarsonjeraView(Garsonjera a)
            {
                this.IdSmestaja = a.IdSmestaja;
                this.ImeVlasnika = a.ImeVlasnika;
                this.PrezimeVlasnika = a.PrezimeVlasnika;
                this.Telefon = a.Telefon;
               
            }

            public GarsonjeraView()
            { }
        }
    }

