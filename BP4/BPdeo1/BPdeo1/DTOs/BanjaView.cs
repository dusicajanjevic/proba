﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BPdeo1.Entiteti;


namespace BPdeo1.DTOs
{
   public class BanjaView
    {

        public int BanjaId { get; set; }
        public string TipOkruzenja { get; set; }


        public BanjaView(Banja b)
        {
            this.BanjaId = b.Id;
            this.TipOkruzenja = "Banja";
            
        }
       public BanjaView()
        { }
    }
}

