﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BPdeo1.Entiteti;
using BPdeo1.DTOs;
using BPdeo1.Data_Providers;

namespace Test.Controllers
{
    public class TrokrevetnaController : ApiController
    {

        public IEnumerable<TrokrevetnaView> Get()
        {
            TrokrevetnaProvider provider = new TrokrevetnaProvider();

            IEnumerable<TrokrevetnaView> sobe = provider.GetTrokrevetne();

            return sobe;
        }

        public TrokrevetnaView Get(int id)
        {
            TrokrevetnaProvider provider = new TrokrevetnaProvider();

            return provider.GetTrokrevetnaView(id);
        }

        public int Post([FromBody]Trokrevetna s)
        {
            TrokrevetnaProvider provider = new TrokrevetnaProvider();

            return provider.AddTrokrevetna(s);
        }

        public int Put([FromBody]Trokrevetna s)
        {
            TrokrevetnaProvider provider = new TrokrevetnaProvider();

            return provider.EditTrokrevetna(s);
        }

        public int Delete(int id)
        {
            TrokrevetnaProvider provider = new TrokrevetnaProvider();

            return provider.RemoveTrokrevetna(id);
        }
    }
}
