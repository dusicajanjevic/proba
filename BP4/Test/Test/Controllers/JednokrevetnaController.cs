﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BPdeo1.Entiteti;
using BPdeo1.DTOs;
using BPdeo1.Data_Providers;

namespace Test.Controllers
{
    public class JednokrevetnaController : ApiController
    {

        public IEnumerable<JednokrevetnaView> Get()
        {
            JednokrevetnaProvider provider = new JednokrevetnaProvider();

            IEnumerable<JednokrevetnaView> sobe = provider.GetJednokrevetne();

            return sobe;
        }

        public JednokrevetnaView Get(int id)
        {
            JednokrevetnaProvider provider = new JednokrevetnaProvider();

            return provider.GetJednokrevetnaView(id);
        }

        public int Post([FromBody]Jednokrevetna s)
        {
            JednokrevetnaProvider provider = new JednokrevetnaProvider();

            return provider.AddJednokrevetna(s);
        }

        public int Put([FromBody]Jednokrevetna s)
        {
            JednokrevetnaProvider provider = new JednokrevetnaProvider();

            return provider.EditJednokrevetna(s);
        }

        public int Delete(int id)
        {
            JednokrevetnaProvider provider = new JednokrevetnaProvider();

            return provider.RemoveJednokrevetna(id);
        }
    }
}
