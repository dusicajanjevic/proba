﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BPdeo1.Entiteti;
using BPdeo1.DTOs;
using BPdeo1.Data_Providers;

namespace Test.Controllers
{
    public class RezervacijaHotelaController : ApiController
    {

        public IEnumerable<RezervacijaHotelaView> Get()
        {
            RezervacijaHotelaProvider provider = new RezervacijaHotelaProvider();

            IEnumerable<RezervacijaHotelaView> rez = provider.GetRezervacijeHotela();

            return rez;
        }

        public RezervacijaHotelaView Get(int id)
        {
            RezervacijaHotelaProvider provider = new RezervacijaHotelaProvider();

            return provider.GetRezervacijaHotelaView(id);
        }

        public int Post([FromBody]RezervacijaHotela r)
        {
            RezervacijaHotelaProvider provider = new RezervacijaHotelaProvider();

            return provider.AddRezervacijaHotela(r);
        }

        public int Put([FromBody]RezervacijaHotela r)
        {
            RezervacijaHotelaProvider provider = new RezervacijaHotelaProvider();

            return provider.EditRezervacijaHotela(r);
        }

        public int Delete(int id)
        {
            RezervacijaHotelaProvider provider = new RezervacijaHotelaProvider();

            return provider.RemoveRezervacijaHotela(id);
        }
    }
}
