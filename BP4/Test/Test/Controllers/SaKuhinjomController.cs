﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BPdeo1.Entiteti;
using BPdeo1.DTOs;
using BPdeo1.Data_Providers;

namespace Test.Controllers
{
    public class SaKuhinjomController : ApiController
    {
        // GET api/apartman
        public IEnumerable<SaKuhinjomView> Get()
        {
            SaKuhinjomProvider provider = new SaKuhinjomProvider();

            IEnumerable<SaKuhinjomView> saK = provider.GetSaKuhinjom();



            return saK;
        }

        public SaKuhinjomView Get(int id)
        {
            SaKuhinjomProvider provider = new SaKuhinjomProvider();

            return provider.GetSaKuhinjomView(id);
        }

        public int Post([FromBody]SaKuhinjom sk)
        {
            SaKuhinjomProvider provider = new SaKuhinjomProvider();

            return provider.AddSaKuhinjom(sk);
        }

        public int Put([FromBody]SaKuhinjom sk)
        {
            SaKuhinjomProvider sp = new SaKuhinjomProvider();
            return sp.EditSaKuhinjom(sk);
        }

        public int Delete(int id)
        {
            SaKuhinjomProvider provider = new SaKuhinjomProvider();

            return provider.RemoveSaKuhinjom(id);
        }

    }
}