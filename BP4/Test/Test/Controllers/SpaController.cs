﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BPdeo1.Entiteti;
using BPdeo1.DTOs;
using BPdeo1.Data_Providers;

namespace Test.Controllers
{
    public class SpaController : ApiController
    {
        public IEnumerable<SpaView> Get()
        {
            SpaProvider provider = new SpaProvider();

            IEnumerable<SpaView> spa = provider.GetSpa();

            return spa;
        }

        public SpaView Get(int id)
        {
            SpaProvider provider = new SpaProvider();

            return provider.GetSpaView(id);
        }

        public int Post([FromBody]Spa spa)
        {
            SpaProvider provider = new SpaProvider();

            return provider.AddSpa(spa);
        }

        public int Put([FromBody]Spa spa)
        {
            SpaProvider provider = new SpaProvider();

            return provider.EditSpa(spa);
        }

        public int Delete(int id)
        {
            SpaProvider provider = new SpaProvider();

            return provider.RemoveSpa(id);
        }
    }
}
