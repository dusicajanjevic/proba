﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BPdeo1.Entiteti;
using BPdeo1.DTOs;
using BPdeo1.Data_Providers;

namespace Test.Controllers
{
    public class GarsonjeraController : ApiController
    {
        // GET api/apartman
        public IEnumerable<GarsonjeraView> Get()
        {
            GarsonjeraProvider provider = new GarsonjeraProvider();

            IEnumerable<GarsonjeraView> garsonjere = provider.GetGarsonjere();



            return garsonjere;
        }

        public GarsonjeraView Get(int id)
        {
            GarsonjeraProvider provider = new GarsonjeraProvider();

            return provider.GetGarsonjeraView(id);
        }

        public int Post([FromBody]Garsonjera g)
        {
            GarsonjeraProvider provider = new GarsonjeraProvider();

            return provider.AddGarsonjera(g);
        }

        public int Put([FromBody]Garsonjera g)
        {
            GarsonjeraProvider gp = new GarsonjeraProvider();
            return gp.EditGarsonjera(g);
        }

        public int Delete(int id)
        {
            GarsonjeraProvider provider = new GarsonjeraProvider();

            return provider.RemoveGarsonjera(id);
        }

    }
}

