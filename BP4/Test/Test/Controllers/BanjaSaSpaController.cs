﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BPdeo1.Entiteti;
using BPdeo1.DTOs;
using BPdeo1.Data_Providers;

namespace Test.Controllers
{
    public class BanjaSaSpaController : ApiController
    {

        public IEnumerable<BanjaSaSpaView> Get()
        {
            BanjaSaSpaProvider provider = new BanjaSaSpaProvider();

            IEnumerable<BanjaSaSpaView> banjeSaSpa = provider.GetBanjaSaSpa();

            return banjeSaSpa;
        }

        public BanjaSaSpaView Get(int id)
        {
            BanjaSaSpaProvider provider = new BanjaSaSpaProvider();

            return provider.GetBanjaSaSpaView(id);
        }

        public int Post([FromBody]BanjaSaSpa b)
        {
            BanjaSaSpaProvider provider = new BanjaSaSpaProvider();

            return provider.AddBanjaSaSpa(b);
        }

        public int Put([FromBody]BanjaSaSpa b)
        {
            BanjaSaSpaProvider provider = new BanjaSaSpaProvider();

            return provider.EditBanjaSaSpa(b);
        }

        public int Delete(int id)
        {
            BanjaSaSpaProvider provider = new BanjaSaSpaProvider();

            return provider.RemoveBanjaSaSpa(id);
        }
    }
}
