﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using BPdeo1.Entiteti;
using BPdeo1.DTOs;
using BPdeo1.Data_Providers;
using System.Web.Http;

namespace Test.Controllers
{
    public class DvokrevetnaController : ApiController
    {

        public IEnumerable<DvokrevetnaView> Get()
        {
            DvokrevetnaProvider provider = new DvokrevetnaProvider();

            IEnumerable<DvokrevetnaView> sobe = provider.GetDvokrevetne();

            return sobe;
        }

        public DvokrevetnaView Get(int id)
        {
            DvokrevetnaProvider provider = new DvokrevetnaProvider();

            return provider.GetDvokrevetnaView(id);
        }

        public int Post([FromBody]Dvokrevetna s)
        {
            DvokrevetnaProvider provider = new DvokrevetnaProvider();

            return provider.AddDvokrevetna(s);
        }

        public int Put([FromBody]Dvokrevetna s)
        {
            DvokrevetnaProvider provider = new DvokrevetnaProvider();

            return provider.EditDvokrevetna(s);
        }

        public int Delete(int id)
        {
            DvokrevetnaProvider provider = new DvokrevetnaProvider();

            return provider.RemoveDvokrevetna(id);
        }
    }
}
