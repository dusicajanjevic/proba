﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BPdeo1.Entiteti;
using BPdeo1.DTOs;
using BPdeo1.Data_Providers;

namespace Test.Controllers
{
    public class BanjaController : ApiController
    {


        public IEnumerable<BanjaView> Get()
        {
            BanjaProvider provider = new BanjaProvider();

            IEnumerable<BanjaView> banje = provider.GetBanja();

            return banje;
        }

        public BanjaView Get(int id)
        {
            BanjaProvider provider = new BanjaProvider();

            return provider.GetBanjaView(id);
        }

        public int Post([FromBody]Banja b)
        {
            BanjaProvider provider = new BanjaProvider();

            return provider.AddBanja(b);
        }

        public int Put([FromBody]Banja b)
        {
            BanjaProvider provider = new BanjaProvider();

            return provider.EditBanja(b);
        }

        public int Delete(int id)
        {
            BanjaProvider provider = new BanjaProvider();

            return provider.RemoveBanja(id);
        }
    }
}
